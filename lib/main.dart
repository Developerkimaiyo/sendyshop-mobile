import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/helper/shareprefs.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/auth_provider.dart';
import 'package:sendyshop/provider/brand_provider.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/provider/category_provider.dart';
import 'package:sendyshop/provider/location_provider.dart';
import 'package:sendyshop/provider/onboarding_provider.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/provider/product_details_provider.dart';
import 'package:sendyshop/provider/product_provider.dart';
import 'package:sendyshop/provider/profile_provider.dart';
import 'package:sendyshop/provider/search_provider.dart';
import 'package:sendyshop/provider/seller_provider.dart';
import 'package:sendyshop/theme/dark_theme.dart';
import 'package:sendyshop/theme/light_theme.dart';
import 'package:sendyshop/util/app_route.dart';
import 'package:sendyshop/util/route_generater.dart';

import 'di_container.dart' as di;

Future<void> initServices() async {
  await SharedPrefs.init();
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initServices();
  await di.init();
  runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (context) => di.sl<OnboardingProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ProfileProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<SearchProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<LocationProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<OrderProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<AuthProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<CartProvider>()),
      ChangeNotifierProvider(
          create: (context) => di.sl<ProductDetailsProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<SellerProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ProductProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<CategoryProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<BrandProvider>()),
      ChangeNotifierProvider(create: (context) => di.sl<ThemeProvider>()),
    ],
    child: FlutterUIKit(),
  ));
}

class FlutterUIKit extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Consumer<ThemeProvider>(builder: (context, themeData, child) {
      return MaterialApp(
        title: 'Sendy Shop',
        debugShowCheckedModeBanner: false,
        theme: themeData.darkTheme ? DarkData : LightData,
        onGenerateRoute: RouteGenerator.generatedRoute,
        initialRoute:
            Provider.of<AuthProvider>(context, listen: false).isLoggedIn()
                ? AppRoutes.MAIN_SCREEN
                : AppRoutes.ONBOARDING_SCREEN,
      );
    });
  }
}
