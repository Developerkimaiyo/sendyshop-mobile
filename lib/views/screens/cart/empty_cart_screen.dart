import 'package:flutter/material.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';

class EmptyCartScreen extends StatelessWidget {
  final Widget child;
  EmptyCartScreen({this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(Images.no_data, width: 150, height: 150),
              Text('sorry',
                  style: rubikBold.copyWith(
                    fontSize: 30,
                    color: ColorResources.GRADIENTSTART,
                  )),
              SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
              Text(
                'Your Cart Is Empty',
                textAlign: TextAlign.center,
                style: rubikRegular,
              ),
              SizedBox(height: 40),
              SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}
