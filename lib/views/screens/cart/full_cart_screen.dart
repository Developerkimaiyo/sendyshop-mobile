import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/cart_model.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/helper/price_converter.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/auth_provider.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/views/basewidget/animated_custom_dialog.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/basewidget/login_dialog.dart';
import 'package:sendyshop/views/basewidget/show_custom_snakbar.dart';
import 'package:sendyshop/views/screens/cart/empty_cart_screen.dart';
import 'package:sendyshop/views/screens/cart/widget/cart_widget.dart';
import 'package:sendyshop/views/screens/checkout/checkout_screen.dart';
import 'package:sendyshop/views/screens/main/bottom_bar.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/orderscreen';
  final bool fromCheckout;
  final List<CartModel> checkoutCartList;
  CartScreen({this.fromCheckout = false, this.checkoutCartList});

  @override
  Widget build(BuildContext context) {
    NetworkInfo.checkConnectivity(context);
    List<CartModel> cartList = [];
    if (fromCheckout) {
      cartList.addAll(checkoutCartList);
    } else {
      cartList.addAll(Provider.of<CartProvider>(context).cartList);
    }

    List<String> sellerList = [];
    List<List<CartModel>> cartProductList = [];
    List<List<int>> cartProductIndexList = [];
    cartList.forEach((cart) {
      if (!sellerList.contains(cart.seller)) {
        sellerList.add(cart.seller);
      }
    });
    sellerList.forEach((seller) {
      List<CartModel> cartLists = [];
      List<int> indexList = [];
      cartList.forEach((cart) {
        if (seller == cart.seller) {
          cartLists.add(cart);
          indexList.add(cartList.indexOf(cart));
        }
      });
      cartProductList.add(cartLists);
      cartProductIndexList.add(indexList);
    });
    return Scaffold(
      bottomNavigationBar: !fromCheckout
          ? Provider.of<CartProvider>(context).cartList.length != 0
              ? Container(
                  height: 60,
                  padding: EdgeInsets.symmetric(
                      horizontal: Dimensions.PADDING_SIZE_LARGE,
                      vertical: Dimensions.PADDING_SIZE_DEFAULT),
                  decoration: BoxDecoration(
                      color: ColorResources.getChatIcon(context),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.grey.withOpacity(0.2),
                            spreadRadius: 1,
                            blurRadius: 7,
                            offset: Offset(0, 1)), // changes position of shadow
                      ],
                      gradient: Provider.of<ThemeProvider>(context).darkTheme
                          ? null
                          : LinearGradient(colors: [
                              Theme.of(context).primaryColor,
                              ColorResources.getPrimary(context),
                              ColorResources.getPrimary(context),
                            ]),
                      borderRadius: BorderRadius.circular(10)),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(
                            child: Center(
                                child: Text(
                          PriceConverter.convertPrice(context,
                              Provider.of<CartProvider>(context).amount),
                          style: rubikSemiBold.copyWith(
                              color: Theme.of(context).accentColor),
                        ))),
                        Builder(
                          builder: (context) => ElevatedButton(
                            onPressed: () {
                              if (Provider.of<AuthProvider>(context,
                                      listen: false)
                                  .isLoggedIn()) {
                                List<CartModel> cartList = [];
                                for (int i = 0;
                                    i <
                                        Provider.of<CartProvider>(context,
                                                listen: false)
                                            .isSelectedList
                                            .length;
                                    i++) {
                                  if (Provider.of<CartProvider>(context,
                                          listen: false)
                                      .isSelectedList[i]) {
                                    cartList.add(Provider.of<CartProvider>(
                                            context,
                                            listen: false)
                                        .cartList[i]);
                                  }
                                }
                                if (cartList.length > 0) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => CheckoutScreen(
                                              cartList: cartList)));
                                } else {
                                  showCustomSnackBar('No product.', context);
                                }
                              } else {
                                showAnimatedDialog(context, LoginDialog(),
                                    isFlip: true);
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            child: Text('checkout',
                                style: rubikSemiBold.copyWith(
                                  fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                                  color: ColorResources.getPrimary(context),
                                )),
                          ),
                        ),
                      ]),
                )
              : CustomButton(
                  buttonText: 'Continue Shopping',
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => BottomBarScreen()));
                  })
          : null,
      body: Column(children: [
        CustomAppBar(
          title: 'Cart',
          isBackButtonExist: false,
        ),
        Provider.of<CartProvider>(context).cartList.length != 0
            ? ListView.builder(
                itemCount: sellerList.length,
                padding: EdgeInsets.all(0),
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Padding(
                    padding:
                        EdgeInsets.only(bottom: Dimensions.PADDING_SIZE_LARGE),
                    child: Column(children: [
                      Container(
                        padding: EdgeInsets.all(Dimensions.MARGIN_SIZE_DEFAULT),
                        decoration: BoxDecoration(
                            color: Theme.of(context).accentColor,
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.grey.withOpacity(0.3),
                                  spreadRadius: 1,
                                  blurRadius: 3,
                                  offset: Offset(0, 3)),
                            ]),
                      ),
                      Consumer<CartProvider>(
                        builder: (context, carProvider, child) {
                          return ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            padding: EdgeInsets.all(0),
                            itemCount: cartProductList[index].length,
                            itemBuilder: (context, i) => CartWidget(
                              cartModel: cartProductList[index][i],
                              index: cartProductIndexList[index][i],
                              fromCheckout: fromCheckout,
                            ),
                          );
                        },
                      ),
                    ]),
                  );
                },
              )
            : Expanded(child: EmptyCartScreen()),
      ]),
    );
  }
}
