import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:list_tile_switch/list_tile_switch.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/location_provider.dart';
import 'package:sendyshop/provider/profile_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/icons.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/screens/account/profile_screen.dart';
import 'package:sendyshop/views/screens/account/widget/sign_out_confirmation_dialog.dart';
import 'package:sendyshop/views/screens/address/address_screen.dart';

class AccountScreen extends StatefulWidget {
  final Function onTap;
  AccountScreen({@required this.onTap});
  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  ScrollController _scrollController;
  var top = 0.0;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context));
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<LocationProvider>(context, listen: false)
            .initAddressList());
    NetworkInfo.checkConnectivity(context);
    _scrollController = ScrollController();
    _scrollController.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    final themeChange = Provider.of<ThemeProvider>(context);

    return Scaffold(
      body: Consumer<ProfileProvider>(builder: (context, profile, child) {
        return Stack(
          children: [
            CustomScrollView(
              controller: _scrollController,
              slivers: <Widget>[
                SliverAppBar(
                  automaticallyImplyLeading: false,
                  elevation: 4,
                  expandedHeight: 200,
                  pinned: true,
                  flexibleSpace: LayoutBuilder(builder:
                      (BuildContext context, BoxConstraints constraints) {
                    top = constraints.biggest.height;
                    return Container(
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              ColorResources.STARTCOLOR,
                              ColorResources.ENDCOLOR,
                            ],
                            begin: const FractionalOffset(0.0, 0.0),
                            end: const FractionalOffset(1.0, 0.0),
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      ),
                      child: FlexibleSpaceBar(
                        collapseMode: CollapseMode.parallax,
                        centerTitle: true,
                        title: Row(
                          //  mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            AnimatedOpacity(
                              duration: Duration(milliseconds: 300),
                              opacity: top <= 110.0 ? 1.0 : 0,
                              child: Row(
                                children: [
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Container(
                                    height: kToolbarHeight / 1.8,
                                    width: kToolbarHeight / 1.8,
                                    child: ClipRRect(
                                        borderRadius: BorderRadius.circular(50),
                                        child: Container(
                                            width: 100,
                                            height: 100,
                                            child: CachedNetworkImage(
                                              imageUrl: Images.user,
                                              placeholder: (context, url) =>
                                                  Image.asset(
                                                      Images.placeholder_image),
                                              errorWidget: (context, url,
                                                      error) =>
                                                  Image.asset(
                                                      Images.placeholder_image),
                                            ))),
                                  ),
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Text(
                                    // 'top.toString()',
                                    '${profile.userInfoModel.fName} ${profile.userInfoModel.lName}',
                                    style: TextStyle(
                                        fontSize: 20.0, color: Colors.white),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        background: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              SizedBox(height: 20),
                              Container(
                                height: 80,
                                width: 80,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    border: Border.all(
                                        color: ColorResources.COLOR_WHITE,
                                        width: 2)),
                                child: Stack(
                                  clipBehavior: Clip.none,
                                  children: [
                                    ClipRRect(
                                        borderRadius: BorderRadius.circular(50),
                                        child: Container(
                                            width: 100,
                                            height: 100,
                                            child: Image.asset(
                                                Images.placeholder_image))),
                                  ],
                                ),
                              ),
                              SizedBox(height: 20),
                              Text(
                                '${profile.userInfoModel.fName} ${profile.userInfoModel.lName}',
                                style: rubikSemiBold.copyWith(
                                    color: ColorResources.COLOR_WHITE,
                                    fontSize: 20.0),
                              )
                            ]),
                      ),
                    );
                  }),
                ),
                SliverToBoxAdapter(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      ListTileSwitch(
                        value: themeChange.darkTheme,
                        leading: Icon(
                          Ionicons.md_moon,
                          color: Theme.of(context).textTheme.bodyText1.color,
                        ),
                        onChanged: (value) {
                          setState(() {
                            themeChange.darkTheme = value;
                          });
                        },
                        visualDensity: VisualDensity.comfortable,
                        switchType: SwitchType.cupertino,
                        switchActiveColor: Theme.of(context).primaryColor,
                        title: Text('Dark theme',
                            style: rubikMedium.copyWith(
                                fontSize: Dimensions.FONT_SIZE_LARGE)),
                      ),
                      SizedBox(height: 30),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Theme.of(context).splashColor,
                          child: ListTile(
                            onTap: () => widget.onTap(1),
                            title: Text('Cart',
                                style: rubikMedium.copyWith(
                                    fontSize: Dimensions.FONT_SIZE_LARGE)),
                            trailing: Icon(
                              Icons.chevron_right_rounded,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                            leading: Icon(
                              MyIcons.cart,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                          ),
                        ),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Theme.of(context).splashColor,
                          child: ListTile(
                            onTap: () => widget.onTap(3),
                            title: Text('Order',
                                style: rubikMedium.copyWith(
                                    fontSize: Dimensions.FONT_SIZE_LARGE)),
                            trailing: Icon(
                              Icons.chevron_right_rounded,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                            leading: Icon(
                              MyIcons.order,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                          ),
                        ),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Theme.of(context).splashColor,
                          child: ListTile(
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => ProfileScreen())),
                            trailing: Icon(
                              Icons.chevron_right_rounded,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                            leading: Icon(
                              MyIcons.user,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                            title: Text('Profile',
                                style: rubikMedium.copyWith(
                                    fontSize: Dimensions.FONT_SIZE_LARGE)),
                          ),
                        ),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Theme.of(context).splashColor,
                          child: ListTile(
                            onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (_) => AddressScreen())),
                            trailing: Icon(
                              Icons.chevron_right_rounded,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                            leading: Icon(
                              Icons.local_shipping,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                            title: Text('Address',
                                style: rubikMedium.copyWith(
                                    fontSize: Dimensions.FONT_SIZE_LARGE)),
                          ),
                        ),
                      ),
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          splashColor: Theme.of(context).splashColor,
                          child: ListTile(
                            onTap: () {
                              showDialog(
                                  context: context,
                                  builder: (context) =>
                                      SignOutConfirmationDialog());
                            },
                            title: Text('Logout',
                                style: rubikMedium.copyWith(
                                    fontSize: Dimensions.FONT_SIZE_LARGE)),
                            leading: Icon(
                              Icons.exit_to_app_rounded,
                              color:
                                  Theme.of(context).textTheme.bodyText1.color,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        );
      }),
    );
  }

  List<IconData> _userTileIcons = [
    Icons.email,
    Icons.phone,
    Icons.local_shipping,
    Icons.watch_later,
    Icons.exit_to_app_rounded
  ];

  Widget userListTile(
      String title, String subTitle, int index, BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        splashColor: Theme.of(context).splashColor,
        child: ListTile(
          onTap: () {},
          title: Text(
            title,
            style: rubikMedium.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
          ),
          subtitle: Text(
            subTitle,
            style:
                rubikRegular.copyWith(fontSize: Dimensions.FONT_SIZE_DEFAULT),
          ),
          leading: Icon(_userTileIcons[index]),
        ),
      ),
    );
  }

  Widget userTitle(String title) {
    return Padding(
      padding: const EdgeInsets.all(14.0),
      child: Text(
        title,
        style: rubikSemiBold.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
      ),
    );
  }
}
