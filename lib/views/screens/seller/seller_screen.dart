import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/seller_model.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/provider/product_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/product_type.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/screens/home/widget/products_view.dart';

class SellerScreen extends StatelessWidget {
  final SellerModel seller;
  SellerScreen({@required this.seller});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProductProvider>(context, listen: false)
            .removeFirstLoading());

    ScrollController _scrollController = ScrollController();
    NetworkInfo.checkConnectivity(context);

    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      body: Column(
        children: [
          Expanded(
            child: ListView(
              controller: _scrollController,
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.all(0),
              children: [
                // Banner
                CustomAppBar(title: seller.fName + ' ' + seller.lName),

                SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                Padding(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
                  child: ProductScreen(
                      productType: ProductType.SELLER_PRODUCT,
                      scrollController: _scrollController,
                      sellerId: seller.id.toString()),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
