import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/address_model.dart';
import 'package:sendyshop/provider/location_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/screens/address/no_address_screen.dart';

import 'add_new_address_screen.dart';

class AddressScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<LocationProvider>(context, listen: false)
            .initAddressList());
    return Scaffold(
      appBar: CustomAppBar(title: 'Address'),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add, color: Colors.white),
        backgroundColor: Theme.of(context).primaryColor,
        onPressed: () => Navigator.of(context)
            .push(MaterialPageRoute(builder: (_) => AddNewAddressScreen())),
      ),
      body: Consumer<LocationProvider>(
        builder: (context, locationProvider, child) {
          return locationProvider.addressList != null
              ? locationProvider.addressList.length > 0
                  ? ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                      itemCount: locationProvider.addressList.length,
                      itemBuilder: (context, index) => addressWidget(
                          addressModel: locationProvider.addressList[index],
                          context: context,
                          index: index),
                    )
                  : EmptyAddressScreen()
              : Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor)));
        },
      ),
    );
  }

  Widget addressWidget(
      {AddressModel addressModel, BuildContext context, int index}) {
    return Consumer<LocationProvider>(builder: (context, address, child) {
      return Container(
        padding: EdgeInsets.symmetric(vertical: 7, horizontal: 7),
        margin: EdgeInsets.only(bottom: 8.0),
        height: 70,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(Dimensions.PADDING_SIZE_SMALL),
            color: ColorResources.getSearchBg(context)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              flex: 2,
              child: Row(
                children: [
                  SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                  Icon(
                    address.addressList[index].addressType == 'Home'
                        ? Icons.home_filled
                        : address.addressList[index].addressType == 'Office'
                            ? Icons.work
                            : Icons.list_alt_rounded,
                    color: ColorResources.getPrimary(context),
                    size: 20,
                  ),
                  SizedBox(width: Dimensions.PADDING_SIZE_DEFAULT),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(address.addressList[index].addressType ?? '',
                            style: rubikSemiBold.copyWith(
                              fontSize: Dimensions.FONT_SIZE_SMALL,
                              color: ColorResources.getPrimary(context),
                            )),
                        Text(
                            '${address.addressList[index].address}, ${address.addressList[index].city}' ??
                                '',
                            style: rubikRegular,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Stack(
              clipBehavior: Clip.none,
              children: [
                Container(
                  margin: EdgeInsets.only(right: 10),
                ),
                Positioned(
                  right: -10,
                  top: 0,
                  bottom: 0,
                  child: PopupMenuButton<String>(
                    padding: EdgeInsets.all(0),
                    onSelected: (String result) {
                      if (result == 'delete') {
                        address.deleteUserAddressByID(context, addressModel.id);
                      } else {
                        Provider.of<LocationProvider>(context, listen: false)
                            .getAddress(context, address.addressList[index].id);
                      }
                    },
                    itemBuilder: (BuildContext c) => <PopupMenuEntry<String>>[
                      PopupMenuItem<String>(
                        value: 'edit',
                        child: Text('edit',
                            style: rubikRegular,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      ),
                      PopupMenuItem<String>(
                        value: 'delete',
                        child: Text('delete',
                            style: rubikRegular,
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis),
                      ),
                    ],
                  ),
                )
              ],
            ),
          ],
        ),
      );
    });
  }
}
