import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/address_model.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/provider/location_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/basewidget/textfield/custom_text_field.dart';
import 'package:sendyshop/views/screens/checkout/checkout_screen.dart';

class AddNewAddressScreen extends StatefulWidget {
  final bool isEnableUpdate;
  final bool fromCheckout;
  final int addressId;
  AddNewAddressScreen(
      {this.isEnableUpdate = false, this.addressId, this.fromCheckout = false});

  @override
  _AddNewAddressScreenState createState() => _AddNewAddressScreenState();
}

class _AddNewAddressScreenState extends State<AddNewAddressScreen> {
  final TextEditingController _locationController = TextEditingController();

  final TextEditingController _cityNameController = TextEditingController();

  final TextEditingController _zipcodeController = TextEditingController();

  final TextEditingController _contactNameController = TextEditingController();

  final TextEditingController _contactPersonNumberController =
      TextEditingController();
  final TextEditingController _addressTypeController = TextEditingController();

  final FocusNode _addressNode = FocusNode();

  final FocusNode _cityNode = FocusNode();

  final FocusNode _zipNode = FocusNode();

  final FocusNode _nameNode = FocusNode();

  final FocusNode _numberNode = FocusNode();

  final FocusNode _typeNode = FocusNode();

  @override
  Widget build(BuildContext context) {
    if (widget.isEnableUpdate) {
      _locationController.text =
          Provider.of<LocationProvider>(context, listen: false)
              .addressModel
              .address;
      _cityNameController.text =
          Provider.of<LocationProvider>(context, listen: false)
              .addressModel
              .city;
      _zipcodeController.text =
          Provider.of<LocationProvider>(context, listen: false)
              .addressModel
              .zip;
      _contactNameController.text =
          Provider.of<LocationProvider>(context, listen: false)
              .addressModel
              .contactName;
      _contactPersonNumberController.text =
          Provider.of<LocationProvider>(context, listen: false)
              .addressModel
              .phone;
      _addressTypeController.text =
          Provider.of<LocationProvider>(context, listen: false)
              .addressModel
              .addressType;
    }

    return Scaffold(
      appBar: CustomAppBar(
          title: widget.isEnableUpdate ? 'Update Address' : 'Add New Address'),
      body: Padding(
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
        child: Consumer<LocationProvider>(
          builder: (context, locationProvider, child) {
            return Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: ListView(
                      physics: BouncingScrollPhysics(),
                      children: [
                        // for label us
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 24.0),
                          child: Text(
                            'Label Location',
                            style: rubikBold.copyWith(
                                color:
                                    ColorResources.getGreyBunkerColor(context),
                                fontSize: Dimensions.FONT_SIZE_LARGE),
                          ),
                        ),

                        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                        CustomTextField(
                          hintText: 'Home/office/other',
                          isShowBorder: true,
                          inputType: TextInputType.text,
                          inputAction: TextInputAction.next,
                          focusNode: _typeNode,
                          nextFocus: _addressNode,
                          controller: _addressTypeController,
                        ),

                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 24.0),
                          child: Text(
                            'Delivery Address',
                            style: rubikBold.copyWith(
                                color:
                                    ColorResources.getGreyBunkerColor(context),
                                fontSize: Dimensions.FONT_SIZE_LARGE),
                          ),
                        ),

                        // for Address Field
                        Text(
                          'Address Line',
                          style: rubikSemiBold.copyWith(
                              color: ColorResources.getHintColor(context)),
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                        CustomTextField(
                          hintText: 'Enter Your Address',
                          isShowBorder: true,
                          inputType: TextInputType.streetAddress,
                          inputAction: TextInputAction.next,
                          focusNode: _addressNode,
                          nextFocus: _cityNode,
                          controller: _locationController,
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
// for City Field
                        Text(
                          'City Name',
                          style: rubikSemiBold.copyWith(
                              color: ColorResources.getHintColor(context)),
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                        CustomTextField(
                          hintText: 'Enter Your City',
                          isShowBorder: true,
                          inputType: TextInputType.text,
                          inputAction: TextInputAction.next,
                          focusNode: _cityNode,
                          nextFocus: _zipNode,
                          controller: _cityNameController,
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
                        // for zip Field
                        Text(
                          'Zip Code',
                          style: rubikSemiBold.copyWith(
                              color: ColorResources.getHintColor(context)),
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                        CustomTextField(
                          hintText: 'Enter Your Zip Code',
                          isShowBorder: true,
                          inputType: TextInputType.text,
                          inputAction: TextInputAction.next,
                          focusNode: _zipNode,
                          nextFocus: _nameNode,
                          controller: _zipcodeController,
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
                        // for Contact Person Name
                        Text(
                          'Contact Person Name',
                          style: rubikSemiBold.copyWith(
                              color: ColorResources.getHintColor(context)),
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                        CustomTextField(
                          hintText: 'Enter Contact Person Name',
                          isShowBorder: true,
                          inputType: TextInputType.name,
                          controller: _contactNameController,
                          focusNode: _nameNode,
                          nextFocus: _numberNode,
                          inputAction: TextInputAction.next,
                          capitalization: TextCapitalization.words,
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),

                        // for Contact Person Number
                        Text(
                          'Contact Person Number',
                          style: rubikSemiBold.copyWith(
                              color: ColorResources.getHintColor(context)),
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                        CustomTextField(
                          hintText: 'Enter Contact Person Number',
                          isShowBorder: true,
                          inputType: TextInputType.phone,
                          inputAction: TextInputAction.done,
                          focusNode: _numberNode,
                          controller: _contactPersonNumberController,
                        ),
                        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),

                        SizedBox(
                          height: Dimensions.PADDING_SIZE_DEFAULT,
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: Dimensions.PADDING_SIZE_DEFAULT,
                ),
                Container(
                  height: 50.0,
                  child: !locationProvider.isLoading
                      ? CustomButton(
                          buttonText: widget.isEnableUpdate
                              ? 'Update Address'
                              : 'Save Location',
                          onTap: () {
                            _addAddress();
                          },
                        )
                      : Center(
                          child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(
                              ColorResources.getPrimary(context)),
                        )),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  _addAddress() {
    if (Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .addressType ==
            _addressTypeController.text &&
        Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .address ==
            _locationController.text &&
        Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .city ==
            _cityNameController.text &&
        Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .zip ==
            _zipcodeController.text &&
        Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .contactName ==
            _contactNameController.text &&
        Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .phone ==
            _contactPersonNumberController.text) {
      Ui.errorSnackBar(context, message: 'Change something to update');
    } else if (_addressTypeController.text.isEmpty) {
      Ui.errorSnackBar(context, message: 'Address type field is required');
    } else if (_locationController.text.isEmpty) {
      Ui.errorSnackBar(context, message: 'Address field is required');
    } else if (_cityNameController.text.isEmpty) {
      Ui.errorSnackBar(context, message: 'City field is required');
    } else if (_zipcodeController.text.isEmpty) {
      Ui.errorSnackBar(context, message: 'Zip code field is required');
    } else if (_contactNameController.text.isEmpty) {
      Ui.errorSnackBar(context, message: 'Contact name field is required');
    } else if (_contactPersonNumberController.text.isEmpty) {
      Ui.errorSnackBar(context, message: 'Contact phone field is required');
    } else {
      AddressModel addressModel = AddressModel();
      addressModel.contactName = _contactNameController.text ?? '';
      addressModel.addressType = _addressTypeController.text;
      addressModel.city = _cityNameController.text ?? '';
      addressModel.address = _locationController.text ?? '';
      addressModel.zip = _zipcodeController.text ?? '';
      addressModel.phone = _contactPersonNumberController.text ?? '';
      if (widget.isEnableUpdate) {
        Provider.of<LocationProvider>(context, listen: false).updateAddress(
            context,
            addressModel,
            Provider.of<LocationProvider>(context, listen: false)
                .addressModel
                .id);
      } else {
        Provider.of<LocationProvider>(context, listen: false)
            .addAddress(context, addressModel);
      }
    }
  }

  void navigateTo(BuildContext ctx, String routeName) {
    Navigator.of(ctx).pushNamed(
      routeName,
    );
  }
}
