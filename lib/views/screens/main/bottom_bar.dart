import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/provider/profile_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/icons.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/screens/account/account_screen.dart';
import 'package:sendyshop/views/screens/cart/full_cart_screen.dart';
import 'package:sendyshop/views/screens/home/home_screen.dart';
import 'package:sendyshop/views/screens/order/order_screen.dart';
import 'package:sendyshop/views/screens/search/search_screen.dart';

class BottomBarScreen extends StatefulWidget {
  @override
  _BottomBarScreenState createState() => _BottomBarScreenState();
}

class _BottomBarScreenState extends State<BottomBarScreen> {
  final PageController _pageController = PageController();
  int _pageIndex = 0;
  List<Widget> _screens;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context));
    super.initState();
    _screens = [
      HomeScreen(onTap: (int pageIndex) {
        _setPage(pageIndex);
      }),
      CartScreen(),
      SearchScreen(),
      OrderScreen(),
      AccountScreen(onTap: (int pageIndex) {
        _setPage(pageIndex);
      }),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: Theme.of(context).primaryColor,
        unselectedItemColor: ColorResources.COLOR_GREY,
        showUnselectedLabels: true,
        currentIndex: _pageIndex,
        type: BottomNavigationBarType.fixed,
        items: [
          _barItem(icon: MyIcons.home, label: 'home', index: 0),
          _barItem(icon: MyIcons.cart, label: 'cart', index: 1),
          _barItem(label: 'search', index: 2),
          _barItem(icon: MyIcons.order, label: 'order', index: 3),
          _barItem(icon: MyIcons.user, label: 'account', index: 4)
        ],
        onTap: (int index) {
          _setPage(index);
        },
      ),
      body: PageView.builder(
        controller: _pageController,
        itemCount: _screens.length,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return _screens[index];
        },
      ),
      floatingActionButtonLocation:
          FloatingActionButtonLocation.miniCenterDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: ColorResources.COLOR_PRIMARY,
        tooltip: 'Search',
        elevation: 5,
        child: (Icon(
          MyIcons.search,
          color: ColorResources.COLOR_WHITE,
        )),
        onPressed: () {
          _setPage(2);
        },
      ),
    );
  }

  BottomNavigationBarItem _barItem({IconData icon, String label, int index}) {
    return BottomNavigationBarItem(
      icon: Stack(
        clipBehavior: Clip.none,
        children: [
          Icon(icon,
              color: index == _pageIndex
                  ? ColorResources.COLOR_PRIMARY
                  : ColorResources.COLOR_GREY,
              size: 25),
          index == 1
              ? Positioned(
                  top: -7,
                  right: -7,
                  child: Container(
                    padding: EdgeInsets.all(4),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, color: Colors.deepOrange),
                    child: Text(
                      Provider.of<CartProvider>(context)
                          .cartList
                          .length
                          .toString(),
                      style: rubikMedium.copyWith(
                          color: ColorResources.COLOR_WHITE, fontSize: 8),
                    ),
                  ),
                )
              : SizedBox(),
        ],
      ),
      label: label,
    );
  }

  void _setPage(int pageIndex) {
    setState(() {
      _pageController.jumpToPage(pageIndex);
      _pageIndex = pageIndex;
    });
  }
}
