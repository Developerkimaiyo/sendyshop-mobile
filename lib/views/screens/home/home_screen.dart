import 'package:backdrop/backdrop.dart';
import 'package:flutter/material.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/product_type.dart';
import 'package:sendyshop/views/basewidget/title_row.dart';
import 'package:sendyshop/views/screens/brand/all_brand_screen.dart';
import 'package:sendyshop/views/screens/home/widget/backLayer_widget.dart';
import 'package:sendyshop/views/screens/home/widget/banner_widget.dart';
import 'package:sendyshop/views/screens/home/widget/brand_widget.dart';
import 'package:sendyshop/views/screens/home/widget/category_widget.dart';
import 'package:sendyshop/views/screens/home/widget/products_view.dart';

class HomeScreen extends StatefulWidget {
  final Function onTap;
  HomeScreen({@required this.onTap});

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

final ScrollController _scrollController = ScrollController();

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: BackdropScaffold(
        headerHeight: MediaQuery.of(context).size.height * 0.25,
        appBar: BackdropAppBar(
          title: Text("Sendy Shop"),
          leading: BackdropToggleButton(
            icon: AnimatedIcons.home_menu,
          ),
          flexibleSpace: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              ColorResources.STARTCOLOR,
              ColorResources.ENDCOLOR
            ])),
          ),
          actions: <Widget>[
            IconButton(
              iconSize: 10,
              padding: const EdgeInsets.all(10),
              icon: CircleAvatar(
                radius: 15,
                backgroundColor: Colors.white,
                child: CircleAvatar(
                  radius: 13,
                  backgroundImage: AssetImage(Images.user_image),
                ),
              ),
              onPressed: () => widget.onTap(4),
            )
          ],
        ),
        backLayer: BackLayerMenuWidget(),
        frontLayer: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            //Banner
            Padding(
              padding: EdgeInsets.zero,
              child: BannerWidget(),
            ),
            // Category

            Padding(
              padding: EdgeInsets.fromLTRB(Dimensions.PADDING_SIZE_SMALL, 20,
                  Dimensions.PADDING_SIZE_SMALL, Dimensions.PADDING_SIZE_SMALL),
              child: TitleRow(
                title: 'Category',
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_SMALL),
              child: CategoryWidget(isHomePage: true),
            ),
            // Brand
            Padding(
              padding: EdgeInsets.fromLTRB(Dimensions.PADDING_SIZE_SMALL, 20,
                  Dimensions.PADDING_SIZE_SMALL, Dimensions.PADDING_SIZE_SMALL),
              child: TitleRow(
                  title: 'Brand',
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (_) => AllBrandScreen()));
                  }),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_SMALL),
              child: BrandWidget(isHomePage: true),
            ),
            // Latest Products
            Padding(
              padding: EdgeInsets.fromLTRB(Dimensions.PADDING_SIZE_SMALL, 20,
                  Dimensions.PADDING_SIZE_SMALL, Dimensions.PADDING_SIZE_SMALL),
              child: TitleRow(title: 'Latest Product'),
            ),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_SMALL),
              child: ProductScreen(
                  productType: ProductType.LATEST_PRODUCT,
                  scrollController: _scrollController),
            ),
          ],
        )),
      ),
    ));
  }
}
