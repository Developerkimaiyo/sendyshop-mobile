import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/material.dart';
import 'package:sendyshop/util/images.dart';

class BannerWidget extends StatefulWidget {
  const BannerWidget({Key key}) : super(key: key);

  @override
  _BannerWidgetState createState() => _BannerWidgetState();
}

class _BannerWidgetState extends State<BannerWidget> {
  List _carouselImages = [
    Images.banner1,
    Images.banner2,
    Images.banner3,
    Images.banner4,
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Container(
        height: 190.0,
        width: double.infinity,
        child: Carousel(
          boxFit: BoxFit.fill,
          autoplay: true,
          animationCurve: Curves.fastOutSlowIn,
          animationDuration: Duration(milliseconds: 1000),
          dotSize: 5.0,
          dotIncreasedColor: Colors.purple,
          dotBgColor: Colors.black.withOpacity(0.2),
          dotPosition: DotPosition.bottomCenter,
          showIndicator: true,
          indicatorBgPadding: 5.0,
          images: [
            ExactAssetImage(_carouselImages[0]),
            ExactAssetImage(_carouselImages[1]),
            ExactAssetImage(_carouselImages[2]),
            ExactAssetImage(_carouselImages[3]),
          ],
        ),
      ),
    );
  }
}
