import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/provider/brand_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/image_display.dart';
import 'package:sendyshop/views/screens/product/widget/brand_product_screen.dart';
import 'package:shimmer/shimmer.dart';

class BrandWidget extends StatelessWidget {
  final bool isHomePage;
  BrandWidget({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<BrandProvider>(context, listen: false).initBrandList());

    return Consumer<BrandProvider>(
      builder: (context, brandProvider, child) {
        return brandProvider.brandList.length != 0
            ? Container(
                height: 210,
                width: MediaQuery.of(context).size.width * 0.95,
                child: Swiper(
                  itemCount: brandProvider.brandList.length == 0
                      ? isHomePage
                          ? brandProvider.brandList.length > 6
                              ? 6
                              : brandProvider.brandList.length
                          : brandProvider.brandList.length
                      : 6,
                  autoplay: true,
                  viewportFraction: 0.8,
                  scale: 0.9,
                  onTap: (index) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (_) => BrandProductScreen(
                                  id: brandProvider.brandList[index].id
                                      .toString(),
                                  name: brandProvider.brandList[index].name,
                                  image: brandProvider.brandList[index].image,
                                )));
                  },
                  itemBuilder: (BuildContext ctx, int index) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Expanded(
                          child: Container(
                            padding:
                                EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                            decoration: BoxDecoration(
                                color: Theme.of(context).accentColor,
                                shape: BoxShape.rectangle,
                                boxShadow: [
                                  BoxShadow(
                                      color: Colors.grey.withOpacity(0.15),
                                      blurRadius: 5,
                                      spreadRadius: 1)
                                ]),
                            child: ClipRRect(
                              child: Container(
                                  color: Colors.transparent,
                                  child: ImageWidget(
                                      url: brandProvider
                                          .brandList[index].image)),
                            ),
                          ),
                        ),
                        Center(
                            child: Text(
                          brandProvider.brandList[index].name,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: rubikBold.copyWith(
                              fontSize: Dimensions.FONT_SIZE_LARGE),
                        )),
                      ],
                    );
                  },
                ),
              )
            : BrandShimmer(isHomePage: isHomePage);
      },
    );
  }
}

class BrandShimmer extends StatelessWidget {
  final bool isHomePage;
  BrandShimmer({@required this.isHomePage});

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      enabled: Provider.of<BrandProvider>(context).brandList.length == 0,
      child: Container(
          height: 150,
          color: ColorResources.COLOR_WHITE,
          margin: EdgeInsets.only(left: 25, right: 25)),
    );
  }
}
