import 'package:flutter/material.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';

class EmptyOrderScreen extends StatelessWidget {
  final Widget child;
  final bool isRunning;
  EmptyOrderScreen({this.child, this.isRunning});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(isRunning ? Images.no_data : Images.binoculars,
                  width: 150, height: 150),
              Text(isRunning ? 'No Order Available' : 'No History Available',
                  style: rubikBold.copyWith(
                    fontSize: 30,
                    color: ColorResources.GRADIENTSTART,
                  )),
              SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
              SizedBox(height: 30),
              SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}
