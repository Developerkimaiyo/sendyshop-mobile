import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/screens/order/widget/cancel_order_view.dart';
import 'package:sendyshop/views/screens/order/widget/delivered_order_view.dart';
import 'package:sendyshop/views/screens/order/widget/processing_order_view.dart';
import 'package:sendyshop/views/screens/order/widget/transit_order_view.dart';

class OrderScreen extends StatefulWidget {
  static const routeName = '/checkoutscreen';
  @override
  _OrderScreenState createState() => _OrderScreenState();
}

class _OrderScreenState extends State<OrderScreen>
    with TickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, initialIndex: 0, vsync: this);
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<OrderProvider>(context, listen: false)
            .getProcessingOrderList());
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<OrderProvider>(context, listen: false)
            .getTransitOrderList());
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<OrderProvider>(context, listen: false).getDeliveredOrder());
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<OrderProvider>(context, listen: false).getCancelOrder());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ColorResources.COLOR_WHITE,
      appBar: CustomAppBar(
        title: 'My Order',
        isBackButtonExist: false,
      ),
      body: Consumer<OrderProvider>(
        builder: (context, order, child) {
          return Column(children: [
            Container(
              color: Theme.of(context).accentColor,
              child: TabBar(
                controller: _tabController,
                labelColor: Theme.of(context).textTheme.bodyText1.color,
                indicatorColor: ColorResources.COLOR_PRIMARY,
                indicatorWeight: 4,
                unselectedLabelStyle: rubikRegular.copyWith(
                    color: ColorResources.COLOR_HINT,
                    fontSize: Dimensions.FONT_SIZE_SMALL),
                labelStyle:
                    rubikMedium.copyWith(fontSize: Dimensions.FONT_SIZE_SMALL),
                tabs: [
                  Tab(text: 'Processing'),
                  Tab(text: 'Transit'),
                  Tab(text: 'Delivered'),
                  Tab(text: 'Cancel'),
                ],
              ),
            ),
            Expanded(
                child: TabBarView(
              controller: _tabController,
              children: [
                ProcessingOrderView(),
                TransitOrderView(),
                DeliveredOrderView(),
                CancelOrderView()
              ],
            )),
          ]);
        },
      ),
    );
  }
}
