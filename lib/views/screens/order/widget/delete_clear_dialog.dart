import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';

class DeleteClearDialog extends StatelessWidget {
  final int orderID;

  DeleteClearDialog({
    @required this.orderID,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Dimensions.PADDING_SIZE_LARGE, vertical: 50),
          child: Text('Are You Sure To Delete ',
              style: rubikBold, textAlign: TextAlign.center),
        ),
        Divider(height: 0, color: ColorResources.getHintColor(context)),
        Row(children: [
          Expanded(
              child: InkWell(
            onTap: () {
              Provider.of<OrderProvider>(context, listen: false)
                  .deleteUserAddressByID(context, orderID);
              Future.delayed(Duration(seconds: 2), () {
                Navigator.pop(context);
              });
            },
            child: Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(10))),
              child: Text('Yes',
                  style:
                      rubikBold.copyWith(color: ColorResources.COLOR_PRIMARY)),
            ),
          )),
          Expanded(
              child: InkWell(
            onTap: () => Navigator.pop(context),
            child: Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorResources.getPrimary(context),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(10))),
              child: Text('No', style: rubikBold.copyWith(color: Colors.white)),
            ),
          )),
        ]),
      ]),
    );
  }
}
