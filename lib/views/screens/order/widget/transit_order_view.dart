import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/image_display.dart';
import 'package:sendyshop/views/screens/order/no_order_screen.dart';
import 'package:sendyshop/views/screens/order/order_track_screen.dart';
import 'package:sendyshop/views/screens/order/widget/order_cancel_dialog.dart';

class TransitOrderView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<OrderProvider>(
        builder: (context, order, index) {
          return order.transitOrderList.length != 0
              ? ListView.builder(
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
                  itemCount: order.transitOrderList.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                      margin: EdgeInsets.only(
                          bottom: Dimensions.PADDING_SIZE_LARGE),
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey[
                                Provider.of<ThemeProvider>(context).darkTheme
                                    ? 700
                                    : 300],
                            spreadRadius: 1,
                            blurRadius: 5,
                          )
                        ],
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(children: [
                        Row(children: [
                          SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                          Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(children: [
                                  Text('${'Order Id'}:',
                                      style: rubikRegular.copyWith(
                                          fontSize:
                                              Dimensions.FONT_SIZE_SMALL)),
                                  SizedBox(
                                      width:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL),
                                  Text(
                                      order.transitOrderList[index].id
                                          .toString(),
                                      style: rubikMedium.copyWith(
                                          fontSize:
                                              Dimensions.FONT_SIZE_SMALL)),
                                ]),
                                SizedBox(
                                    height:
                                        Dimensions.PADDING_SIZE_EXTRA_SMALL),
                                Row(children: [
                                  Text('Payment:',
                                      style: rubikRegular.copyWith(
                                          fontSize:
                                              Dimensions.FONT_SIZE_SMALL)),
                                  SizedBox(
                                      width:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL),
                                  Text(
                                      order
                                          .transitOrderList[index].paymentStatus
                                          .toString(),
                                      style: rubikMedium.copyWith(
                                          fontSize:
                                              Dimensions.FONT_SIZE_SMALL)),
                                ]),
                                SizedBox(
                                    height:
                                        Dimensions.PADDING_SIZE_EXTRA_SMALL),
                                Row(children: [
                                  Icon(Icons.check_circle,
                                      color: ColorResources.COLOR_PRIMARY,
                                      size: 15),
                                  SizedBox(
                                      width:
                                          Dimensions.PADDING_SIZE_EXTRA_SMALL),
                                  Text(
                                    '${order.transitOrderList[index].orderStatus[0].toUpperCase()}${order.transitOrderList[index].orderStatus.substring(1).replaceAll('_', ' ')}',
                                    style: rubikRegular.copyWith(
                                        color: ColorResources.COLOR_PRIMARY),
                                  ),
                                ]),
                              ]),
                        ]),
                        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
                        SizedBox(
                          height: 50,
                          child: Row(children: [
                            Expanded(
                                child: CustomButton(
                              buttonText: 'Cancel Order',
                              onTap: () async {
                                showDialog(
                                    context: context,
                                    builder: (context) => OrderCancelDialog(
                                        orderID:
                                            order.transitOrderList[index].id,
                                        OrderAmount: order
                                            .transitOrderList[index]
                                            .orderAmount,
                                        OrderStatus: "cancel",
                                        PaymentStatus: order
                                            .transitOrderList[index]
                                            .paymentStatus,
                                        TotalTaxAmount: order
                                            .transitOrderList[index]
                                            .totalTaxAmount,
                                        DeliveryAddressId: order
                                            .transitOrderList[index]
                                            .deliveryAddressId));
                              },
                            )),
                            SizedBox(width: 30),
                            Expanded(
                                child: CustomButton(
                              buttonText: 'Track Order',
                              onTap: () async {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => OrderTrackingScreen(
                                            orderID: order
                                                .transitOrderList[index].id
                                                .toString())));
                              },
                            )),
                          ]),
                        ),
                      ]),
                    );
                  },
                )
              : Center(
                  child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor)));
        },
      ),
    );
  }
}
