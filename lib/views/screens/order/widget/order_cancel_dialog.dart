import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/order_model.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';

class OrderCancelDialog extends StatelessWidget {
  final int orderID;
  final String OrderAmount;
  final String OrderStatus;
  final String PaymentStatus;
  final String TotalTaxAmount;
  final String DeliveryAddressId;
  OrderCancelDialog(
      {@required this.orderID,
      @required this.OrderAmount,
      @required this.OrderStatus,
      @required this.PaymentStatus,
      @required this.TotalTaxAmount,
      @required this.DeliveryAddressId});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      child: Column(mainAxisSize: MainAxisSize.min, children: [
        Padding(
          padding: EdgeInsets.symmetric(
              horizontal: Dimensions.PADDING_SIZE_LARGE, vertical: 50),
          child: Text('Are You Sure To Cancel',
              style: rubikBold, textAlign: TextAlign.center),
        ),
        Divider(height: 0, color: ColorResources.getHintColor(context)),
        Row(children: [
          Expanded(
              child: InkWell(
            onTap: () {
              OrderModel orderModel = OrderModel();
              orderModel.orderAmount = OrderAmount ?? '';
              orderModel.paymentStatus = PaymentStatus ?? '';
              orderModel.orderStatus = OrderStatus ?? '';
              orderModel.totalTaxAmount = TotalTaxAmount ?? '';
              orderModel.deliveryAddressId = DeliveryAddressId ?? '';

              Provider.of<OrderProvider>(context, listen: false)
                  .updateDataStatus(context, orderModel, orderID);
            },
            child: Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(10))),
              child: Text('yes',
                  style:
                      rubikBold.copyWith(color: ColorResources.COLOR_PRIMARY)),
            ),
          )),
          Expanded(
              child: InkWell(
            onTap: () => Navigator.pop(context),
            child: Container(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: ColorResources.getPrimary(context),
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(10))),
              child: Text('no', style: rubikBold.copyWith(color: Colors.white)),
            ),
          )),
        ]),
      ]),
    );
  }
}
