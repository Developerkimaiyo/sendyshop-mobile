import 'package:flutter/material.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/screens/main/bottom_bar.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/screens/order/order_track_screen.dart';

class OrderSuccessfulScreen extends StatelessWidget {
  static const routeName = '/ordersuccess';
  final String orderID;
  final int status;
  OrderSuccessfulScreen({this.orderID, this.status = 0});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
          height: 100,
          width: 100,
          decoration: BoxDecoration(
            color: ColorResources.getPrimary(context).withOpacity(0.2),
            shape: BoxShape.circle,
          ),
          child: Icon(
            status == 0
                ? Icons.check_circle
                : status == 1
                    ? Icons.sms_failed
                    : Icons.cancel,
            color: ColorResources.getPrimary(context),
            size: 80,
          ),
        ),
        SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
        Text(
          status == 0
              ? 'order placed successfully'
              : status == 1
                  ? 'order_failed'
                  : 'order_cancelled',
          style: rubikMedium.copyWith(fontSize: Dimensions.FONT_SIZE_LARGE),
        ),
        SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
        Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text('${'order id'}:',
              style:
                  rubikRegular.copyWith(fontSize: Dimensions.FONT_SIZE_SMALL)),
          SizedBox(width: Dimensions.PADDING_SIZE_EXTRA_SMALL),
          Text(orderID,
              style:
                  rubikMedium.copyWith(fontSize: Dimensions.FONT_SIZE_SMALL)),
        ]),
        SizedBox(height: 30),
        Padding(
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
          child: CustomButton(
              buttonText: status == 0 ? 'track order' : 'back home',
              onTap: () {
                if (status == 0) {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                              OrderTrackingScreen(orderID: orderID)));
                } else {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (_) => BottomBarScreen()),
                      (route) => false);
                }
              }),
        ),
      ]),
    );
  }
}
