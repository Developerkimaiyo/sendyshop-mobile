import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/onboarding_provider.dart';
import 'package:sendyshop/util/app_constants.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/screens/auth/auth_screen.dart';

class SendyShopOnBoardingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<OnboardingProvider>(context, listen: false)
            .initializeBoardingList());
    return Scaffold(
      backgroundColor: ColorResources.getBackgroundColor(context),
      body: Consumer<OnboardingProvider>(
        builder: (context, onBoardingList, child) => Column(
          children: [
            Expanded(
              child: PageView.builder(
                itemCount: onBoardingList.onBoardingList.length,
                itemBuilder: (context, index) {
                  return SafeArea(
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Container(
                          margin: EdgeInsets.only(
                              top: Dimensions.PADDING_SIZE_EXTRA_LARGE,
                              left: Dimensions.PADDING_SIZE_DEFAULT,
                              right: Dimensions.PADDING_SIZE_DEFAULT),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: _pageIndicators(
                                onBoardingList.onBoardingList,
                                onBoardingList.selectedIndex,
                                context),
                          ),
                        ),
                        Positioned(
                          top: 150,
                          left: Dimensions.PADDING_SIZE_EXTRA_LARGE,
                          right: Dimensions.PADDING_SIZE_EXTRA_LARGE,
                          child: Image.asset(
                            onBoardingList
                                .onBoardingList[onBoardingList.selectedIndex]
                                .imageUrl,
                            fit: BoxFit.scaleDown,
                            height: 200,
                          ),
                        ),
                        Positioned(
                          bottom: 10,
                          left: 0,
                          right: 0,
                          child: Container(
                            margin: EdgeInsets.only(
                              left: Dimensions.PADDING_SIZE_LARGE,
                              right: Dimensions.PADDING_SIZE_LARGE,
                              bottom: Dimensions.PADDING_SIZE_LARGE,
                              top: 100,
                            ),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(12)),
                            child: Column(
                              children: [
                                Text(
                                  onBoardingList
                                      .onBoardingList[
                                          onBoardingList.selectedIndex]
                                      .title,
                                  style: rubikBold.copyWith(
                                    fontSize: 25,
                                    color: Provider.of<ThemeProvider>(context)
                                            .darkTheme
                                        ? ColorResources.COLOR_WHITE
                                        : ColorResources.COLOR_GREY_BUNKER,
                                  ),
                                ),
                                Text(
                                  onBoardingList
                                      .onBoardingList[
                                          onBoardingList.selectedIndex]
                                      .subTitle,
                                  textAlign: TextAlign.center,
                                  style: rubikRegular.copyWith(
                                    color: Provider.of<ThemeProvider>(context)
                                            .darkTheme
                                        ? ColorResources.COLOR_WHITE
                                        : ColorResources.COLOR_GREY_CHATEAU,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                },
                onPageChanged: onBoardingList.changeOnboardingIndex,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, bottom: 40),
              child: CustomButton(
                buttonText: onBoardingList.selectedIndex ==
                        onBoardingList.onBoardingList.length - 1
                    ? AppConstants.shopping
                    : AppConstants.shopping,
                onTap: () {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => AuthScreen()));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _pageIndicators(
      var onboardingList, int index, BuildContext context) {
    List<Container> _indicators = [];
    for (int i = 0; i < onboardingList.length; i++) {
      _indicators.add(Container(
        width: MediaQuery.of(context).size.width / onboardingList.length - 10,
        height: 3,
        decoration: BoxDecoration(
            color: i == index
                ? ColorResources.COLOR_PRIMARY
                : ColorResources.COLOR_WHITE,
            borderRadius: BorderRadius.circular(5)),
      ));
    }
    return _indicators;
  }
}
