import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/provider/search_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/product_shimmer.dart';
import 'package:sendyshop/views/basewidget/search_widget.dart';
import 'package:sendyshop/views/screens/search/widget/no_search_data_screen.dart';
import 'package:sendyshop/views/screens/search/widget/search_product_widget.dart';

class SearchScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    NetworkInfo.checkConnectivity(context);
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<SearchProvider>(context, listen: false)
            .cleanSearchProduct());
    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      body: Column(
        children: [
          SizedBox(height: 8),
          // for tool bar
          SearchWidget(
            hintText: 'Search Hint',
            onSubmit: (String text) {
              Provider.of<SearchProvider>(context, listen: false)
                  .searchProduct(text);
              Provider.of<SearchProvider>(context, listen: false)
                  .saveSearchAddress(text);
            },
            onClearPressed: () {
              Provider.of<SearchProvider>(context, listen: false)
                  .cleanSearchProduct();
            },
          ),

          Consumer<SearchProvider>(
            builder: (context, searchProvider, child) {
              return !searchProvider.isClear
                  ? searchProvider.searchProductList != null
                      ? searchProvider.searchProductList.length > 0
                          ? Expanded(
                              child: SearchProductWidget(
                                  products: searchProvider.searchProductList,
                                  isViewScrollable: true))
                          : Expanded(child: EmptySearchScreen())
                      : Expanded(
                          child: ProductShimmer(
                              isEnabled: Provider.of<SearchProvider>(context)
                                      .searchProductList ==
                                  null))
                  : Expanded(
                      flex: 4,
                      child: Container(
                        padding:
                            EdgeInsets.all(Dimensions.PADDING_SIZE_DEFAULT),
                        child: Stack(
                          clipBehavior: Clip.none,
                          children: [
                            Consumer<SearchProvider>(
                              builder: (context, searchProvider, child) =>
                                  StaggeredGridView.countBuilder(
                                crossAxisCount: 3,
                                physics: NeverScrollableScrollPhysics(),
                                itemCount: searchProvider.historyList.length,
                                itemBuilder: (context, index) => Container(
                                    alignment: Alignment.center,
                                    child: InkWell(
                                      onTap: () {
                                        Provider.of<SearchProvider>(context,
                                                listen: false)
                                            .searchProduct(searchProvider
                                                .historyList[index]);
                                      },
                                      borderRadius: BorderRadius.circular(20),
                                      child: Container(
                                        padding: EdgeInsets.only(
                                            left: 10,
                                            right: 10,
                                            top: 2,
                                            bottom: 2),
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(16),
                                            color: ColorResources.getGrey(
                                                context)),
                                        width: double.infinity,
                                        child: Center(
                                          child: Text(
                                            Provider.of<SearchProvider>(context,
                                                        listen: false)
                                                    .historyList[index] ??
                                                "",
                                            style: rubikItalic.copyWith(
                                                fontSize:
                                                    Dimensions.FONT_SIZE_SMALL),
                                          ),
                                        ),
                                      ),
                                    )),
                                staggeredTileBuilder: (int index) =>
                                    new StaggeredTile.fit(1),
                                mainAxisSpacing: 4.0,
                                crossAxisSpacing: 4.0,
                              ),
                            ),
                            Positioned(
                              top: -5,
                              left: 0,
                              right: 0,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Search History', style: rubikBold),
                                  InkWell(
                                      borderRadius: BorderRadius.circular(10),
                                      onTap: () {
                                        Provider.of<SearchProvider>(context,
                                                listen: false)
                                            .clearSearchAddress();
                                      },
                                      child: Container(
                                          padding: EdgeInsets.all(5),
                                          child: Text(
                                            'Remove',
                                            style: rubikRegular.copyWith(
                                                fontSize:
                                                    Dimensions.FONT_SIZE_SMALL,
                                                color: ColorResources
                                                    .COLOR_PRIMARY),
                                          )))
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
            },
          ),
        ],
      ),
    );
  }
}
