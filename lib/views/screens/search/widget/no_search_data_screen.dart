import 'package:flutter/material.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';

class EmptySearchScreen extends StatelessWidget {
  final Widget child;

  EmptySearchScreen({this.child});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(Images.clock, width: 150, height: 150),
              Text('No search data available',
                  style: rubikBold.copyWith(
                    fontSize: 30,
                    color: ColorResources.GRADIENTSTART,
                  )),
              SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
              Text(
                'Try Again',
                textAlign: TextAlign.center,
                style: rubikRegular,
              ),
              SizedBox(height: 40),
              SizedBox.shrink(),
            ],
          ),
        ),
      ),
    );
  }
}
