import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/provider/seller_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/title_row.dart';
import 'package:sendyshop/views/screens/seller/seller_screen.dart';

class SellerView extends StatelessWidget {
  final int sellerId;
  SellerView({@required this.sellerId});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<SellerProvider>(context, listen: false)
            .initSeller(context, sellerId.toString()));

    return Consumer<SellerProvider>(
      builder: (context, seller, child) {
        return Container(
          margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
          padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
          color: Theme.of(context).accentColor,
          child: Column(children: [
            TitleRow(title: 'Seller', isDetailsPage: true),
            SizedBox(height: 10),
            Row(children: [
              Expanded(
                child: InkWell(
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                              SellerScreen(seller: seller.sellerModel))),
                  child: Text(
                    seller.sellerModel != null
                        ? seller.sellerModel.fName.toLowerCase() +
                            ' ' +
                            seller.sellerModel.lName.toLowerCase()
                        : '',
                    style: rubikSemiBold.copyWith(
                        fontSize: Dimensions.FONT_SIZE_LARGE,
                        color: ColorResources.getSellerTxt(context)),
                  ),
                ),
              ),
            ]),
          ]),
        );
      },
    );
  }
}
