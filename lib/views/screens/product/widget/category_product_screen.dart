import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/provider/product_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/basewidget/product_shimmer.dart';
import 'package:sendyshop/views/basewidget/product_widget.dart';
import 'package:sendyshop/views/screens/product/no_product_screen.dart';

class CategoryProductScreen extends StatelessWidget {
  final int id;
  final String name;
  final String image;
  CategoryProductScreen({@required this.id, @required this.name, this.image});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProductProvider>(context, listen: false)
            .initCategoryProductList(context, id.toString()));

    return Scaffold(
      backgroundColor: ColorResources.getIconBg(context),
      body: Consumer<ProductProvider>(
        builder: (context, productProvider, child) {
          return Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                CustomAppBar(title: name),
                SizedBox(height: Dimensions.PADDING_SIZE_SMALL),
                !productProvider.firstLoading
                    ? productProvider.categoryProductList.length != 0
                        ? Expanded(
                            child: StaggeredGridView.countBuilder(
                              padding: EdgeInsets.symmetric(
                                  horizontal: Dimensions.PADDING_SIZE_SMALL),
                              physics: BouncingScrollPhysics(),
                              crossAxisCount: 2,
                              itemCount:
                                  productProvider.categoryProductList.length,
                              shrinkWrap: true,
                              staggeredTileBuilder: (int index) =>
                                  StaggeredTile.fit(1),
                              itemBuilder: (BuildContext context, int index) {
                                return ProductWidget(
                                    productModel: productProvider
                                        .categoryProductList[index]);
                              },
                            ),
                          )
                        : EmptyScreen()
                    : ProductShimmer(isEnabled: productProvider.firstLoading),
              ]);
        },
      ),
    );
  }
}
