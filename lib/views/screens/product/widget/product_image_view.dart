import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/product_details_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/views/basewidget/image_display.dart';

class ProductImageView extends StatelessWidget {
  final Product productModel;
  ProductImageView({@required this.productModel});

  final PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20)),
            boxShadow: [
              BoxShadow(color: Colors.grey[300], spreadRadius: 1, blurRadius: 5)
            ],
            gradient: Provider.of<ThemeProvider>(context).darkTheme
                ? null
                : LinearGradient(
                    colors: [
                      ColorResources.COLOR_WHITE,
                      ColorResources.COLOR_WHITE
                    ],
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                  ),
          ),
          child: Stack(children: [
            SizedBox(
              height: MediaQuery.of(context).size.width - 100,
              child: PageView.builder(
                controller: _controller,
                itemCount: 1,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: EdgeInsets.fromLTRB(20, 20, 20, 50),
                    child: Container(
                      height: MediaQuery.of(context).size.width,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: ColorResources.getIconBg(context),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: ImageWidget(
                        url: productModel.thumbnail,
                      ),
                    ),
                  );
                },
                onPageChanged: (index) {
                  Provider.of<ProductDetailsProvider>(context, listen: false)
                      .setImageSliderSelectedIndex(index);
                },
              ),
            ),
          ]),
        ),
      ],
    );
  }
}
