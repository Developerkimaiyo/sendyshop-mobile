import 'package:flutter/material.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/views/basewidget/title_row.dart';
import 'package:sendyshop/views/screens/product/specification_screen.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'dart:convert';

class ProductSpecification extends StatelessWidget {
  final String productSpecification;
  ProductSpecification({@required this.productSpecification});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TitleRow(
            title: 'Product details',
            isDetailsPage: true,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => SpecificationScreen(
                          specification: productSpecification)));
            }),
        SizedBox(height: Dimensions.PADDING_SIZE_EXTRA_SMALL),
        productSpecification.isNotEmpty
            ? SizedBox(
                height: 100,
                child: WebView(
                  javascriptMode: JavascriptMode.unrestricted,
                  initialUrl:
                      'data:text/html;base64,${base64Encode(const Utf8Encoder().convert(productSpecification))}',
                ),
              )
            : Center(child: Text('No specification')),
      ],
    );
  }
}
