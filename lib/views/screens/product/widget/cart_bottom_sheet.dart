import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/cart_model.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/helper/price_converter.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/provider/product_details_provider.dart';
import 'package:sendyshop/provider/seller_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';

class CartBottomSheet extends StatelessWidget {
  final Product product;
  final Function callback;
  CartBottomSheet({@required this.product, this.callback});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<SellerProvider>(context, listen: false)
            .initSeller(context, product.userId.toString()));
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Container(
          color: Color(0xff757575),
          child: Container(
            padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
            decoration: BoxDecoration(
              color: Theme.of(context).accentColor,
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(20), topLeft: Radius.circular(20)),
            ),
            child: Consumer<ProductDetailsProvider>(
              builder: (context, details, child) {
                double price = double.parse(product.unitPrice);

                double priceWithDiscount = PriceConverter.convertWithDiscount(
                    context, price, product.discount, product.discountType);
                double priceWithQuantity = priceWithDiscount * details.quantity;

                return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // Close Button
                      Align(
                          alignment: Alignment.centerRight,
                          child: InkWell(
                            onTap: () => Navigator.pop(context),
                            child: Container(
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Theme.of(context).accentColor,
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey[200],
                                      spreadRadius: 1,
                                      blurRadius: 5,
                                    )
                                  ]),
                              child: Icon(Icons.clear,
                                  size: Dimensions.ICON_SIZE_SMALL),
                            ),
                          )),

                      // Quantity
                      Row(children: [
                        Text('Quantity', style: rubikBold),
                        QuantityButton(
                            isIncrement: false, quantity: details.quantity),
                        Text(details.quantity.toString(), style: rubikSemiBold),
                        QuantityButton(
                            isIncrement: true, quantity: details.quantity),
                      ]),

                      SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                      Row(children: [
                        Text('Total price', style: rubikBold),
                        SizedBox(width: Dimensions.PADDING_SIZE_SMALL),
                        Text(
                          PriceConverter.convertPrice(
                              context, priceWithQuantity),
                          style: rubikBold.copyWith(
                              color: ColorResources.getPrimary(context),
                              fontSize: 16),
                        ),
                      ]),
                      SizedBox(height: Dimensions.PADDING_SIZE_SMALL),

                      // Cart button
                      CustomButton(
                          buttonText: 'Add to cart',
                          onTap: () {
                            CartModel _cartModel = CartModel(
                              product.id,
                              product.thumbnail,
                              product.name,
                              '${Provider.of<SellerProvider>(context, listen: false).sellerModel.fName} ${Provider.of<SellerProvider>(context, listen: false).sellerModel.lName}',
                              price,
                              priceWithDiscount,
                              details.quantity,
                              double.parse(product.discount),
                              product.discountType,
                              double.parse(product.tax),
                              product.taxType,
                            );
                            Navigator.pop(context);

                            Provider.of<CartProvider>(context, listen: false)
                                .addToCart(_cartModel);
                            callback();
                          }),
                    ]);
              },
            ),
          ),
        ),
      ],
    );
  }
}

class QuantityButton extends StatelessWidget {
  final bool isIncrement;
  final int quantity;
  final bool isCartWidget;

  QuantityButton({
    @required this.isIncrement,
    @required this.quantity,
    this.isCartWidget = false,
  });

  @override
  Widget build(BuildContext context) {
    return IconButton(
      onPressed: () {
        if (!isIncrement && quantity > 1) {
          Provider.of<ProductDetailsProvider>(context, listen: false)
              .setQuantity(quantity - 1);
        } else if (isIncrement) {
          Provider.of<ProductDetailsProvider>(context, listen: false)
              .setQuantity(quantity + 1);
        }
      },
      icon: Icon(
        isIncrement ? Icons.add_circle : Icons.remove_circle,
        color: isIncrement
            ? ColorResources.getPrimary(context)
            : quantity > 1
                ? ColorResources.getPrimary(context)
                : ColorResources.getLowGreen(context),
        size: isCartWidget ? 26 : 20,
      ),
    );
  }
}
