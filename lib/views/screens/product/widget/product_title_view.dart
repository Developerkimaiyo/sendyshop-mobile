import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/helper/price_converter.dart';
import 'package:sendyshop/provider/product_details_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:share/share.dart';

class ProductTitleView extends StatelessWidget {
  final Product productModel;
  ProductTitleView({@required this.productModel});

  @override
  Widget build(BuildContext context) {
    double _startingPrice;
    double _endingPrice;

    _startingPrice = double.parse(productModel.unitPrice);

    return Container(
      color: Theme.of(context).accentColor,
      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
      child: Consumer<ProductDetailsProvider>(
        builder: (context, details, child) {
          return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(children: [
                  Text(
                    '${PriceConverter.convertPrice(context, _startingPrice, discount: productModel.discount, discountType: productModel.discountType)}'
                    '${_endingPrice != null ? ' - ${PriceConverter.convertPrice(context, _endingPrice, discount: productModel.discount, discountType: productModel.discountType)}' : ''}',
                    style: rubikBold.copyWith(
                        color: ColorResources.getPrimary(context),
                        fontSize: Dimensions.FONT_SIZE_LARGE),
                  ),
                  SizedBox(width: 20),
                  double.parse(productModel.discount) >= 1
                      ? Container(
                          width: 50,
                          height: 20,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                            border: Border.all(
                                width: 1,
                                color: ColorResources.getPrimary(context)),
                            borderRadius: BorderRadius.circular(50),
                          ),
                          child: Text(
                            PriceConverter.percentageCalculation(
                                context,
                                productModel.unitPrice,
                                productModel.discount,
                                productModel.discountType),
                            style: rubikRegular.copyWith(
                                color: Theme.of(context).hintColor,
                                fontSize: 8),
                          ),
                        )
                      : SizedBox.shrink(),
                  Expanded(child: SizedBox.shrink()),
                  InkWell(
                    onTap: () {
                      if (Provider.of<ProductDetailsProvider>(context,
                                  listen: false)
                              .sharableLink !=
                          null) {
                        Share.share(Provider.of<ProductDetailsProvider>(context,
                                listen: false)
                            .sharableLink);
                      }
                    },
                    child: Container(
                      width: 30,
                      height: 30,
                      decoration: BoxDecoration(
                        color: Theme.of(context).accentColor,
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey[200],
                              spreadRadius: 1,
                              blurRadius: 5)
                        ],
                        shape: BoxShape.circle,
                      ),
                      child: Icon(Icons.share,
                          color: ColorResources.getPrimary(context),
                          size: Dimensions.ICON_SIZE_SMALL),
                    ),
                  ),
                ]),
                Text(
                  '${PriceConverter.convertPrice(context, _startingPrice)}'
                  '${_endingPrice != null ? ' - ${PriceConverter.convertPrice(context, _endingPrice)}' : ''}',
                  style: rubikRegular.copyWith(
                      color: Theme.of(context).hintColor,
                      decoration: TextDecoration.lineThrough),
                ),
                Text(productModel.name ?? '',
                    style: rubikSemiBold.copyWith(
                        fontSize: Dimensions.FONT_SIZE_LARGE),
                    maxLines: 2),
              ]);
        },
      ),
    );
  }
}
