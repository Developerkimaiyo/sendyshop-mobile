import 'package:flutter/material.dart';

import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/provider/product_provider.dart';
import 'package:sendyshop/views/basewidget/product_shimmer.dart';
import 'package:sendyshop/views/basewidget/product_widget.dart';

class RelatedProductView extends StatelessWidget {
  final int categoryId;
  RelatedProductView({@required this.categoryId});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProductProvider>(context, listen: false)
            .initRelatedProductList(context, categoryId.toString()));

    return Consumer<ProductProvider>(
      builder: (context, prodProvider, child) {
        return Column(children: [
          prodProvider.relatedProductList != null
              ? prodProvider.relatedProductList.length != 0
                  ? StaggeredGridView.countBuilder(
                      crossAxisCount: 2,
                      itemCount: prodProvider.relatedProductList.length,
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      staggeredTileBuilder: (int index) => StaggeredTile.fit(1),
                      itemBuilder: (BuildContext context, int index) {
                        return ProductWidget(
                            productModel:
                                prodProvider.relatedProductList[index]);
                      },
                    )
                  : Center(child: Text('No Related Product'))
              : ProductShimmer(
                  isEnabled: Provider.of<ProductProvider>(context)
                          .relatedProductList ==
                      null),
        ]);
      },
    );
  }
}
