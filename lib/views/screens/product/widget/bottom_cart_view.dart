import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/screens/cart/full_cart_screen.dart';
import 'package:sendyshop/views/screens/product/widget/cart_bottom_sheet.dart';

class BottomCartView extends StatelessWidget {
  final Product product;
  BottomCartView({@required this.product});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      decoration: BoxDecoration(
        color: Theme.of(context).accentColor,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10)),
        boxShadow: [
          BoxShadow(color: Colors.grey[300], blurRadius: 15, spreadRadius: 1)
        ],
      ),
      child: Row(children: [
        Expanded(
            flex: 3,
            child: Padding(
              padding: EdgeInsets.all(Dimensions.PADDING_SIZE_EXTRA_SMALL),
              child: Stack(children: [
                GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => CartScreen()));
                    },
                    child: Image.asset(Images.cart_image,
                        color: ColorResources.getPrimary(context))),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Container(
                    height: 17,
                    width: 17,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: ColorResources.getPrimary(context),
                    ),
                    child: Text(
                      Provider.of<CartProvider>(context)
                          .cartList
                          .length
                          .toString(),
                      style: rubikSemiBold.copyWith(
                          fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                          color: Theme.of(context).accentColor),
                    ),
                  ),
                )
              ]),
            )),
        Expanded(
            flex: 11,
            child: InkWell(
              onTap: () {
                if (!Provider.of<CartProvider>(context, listen: false)
                    .isAddedInCart(product.id)) {
                  showModalBottomSheet(
                      context: context,
                      isScrollControlled: true,
                      builder: (con) => CartBottomSheet(
                          product: product,
                          callback: () {
                            Ui.successSnackBar(context,
                                message: 'Added to cart');
                          }));
                } else {
                  Ui.errorSnackBar(context, message: 'Already added');
                }
              },
              child: Container(
                height: 50,
                margin: EdgeInsets.symmetric(horizontal: 5),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: ColorResources.getPrimary(context),
                ),
                child: Text(
                  Provider.of<CartProvider>(context).isAddedInCart(product.id)
                      ? 'Added to cart'
                      : 'Add to cart',
                  style: rubikSemiBold.copyWith(
                      fontSize: Dimensions.FONT_SIZE_LARGE,
                      color: Theme.of(context).accentColor),
                ),
              ),
            )),
      ]),
    );
  }
}
