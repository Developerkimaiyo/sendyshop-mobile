import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/provider/product_details_provider.dart';
import 'package:sendyshop/provider/product_provider.dart';
import 'package:sendyshop/provider/seller_provider.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/basewidget/title_row.dart';
import 'package:sendyshop/views/screens/product/seller_view.dart';
import 'package:sendyshop/views/screens/product/widget/bottom_cart_view.dart';
import 'package:sendyshop/views/screens/product/widget/product_image_view.dart';
import 'package:sendyshop/views/screens/product/widget/product_specification_view.dart';
import 'package:sendyshop/views/screens/product/widget/product_title_view.dart';
import 'package:sendyshop/views/screens/product/widget/related_product_view.dart';

class ProductDetails extends StatelessWidget {
  final Product product;
  ProductDetails({@required this.product});

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProductProvider>(context, listen: false)
            .initRelatedProductList(context, product.userId.toString()));

    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<SellerProvider>(context, listen: false)
            .initSeller(context, product.userId.toString()));

    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<ProductDetailsProvider>(context, listen: false)
            .getSharableLink(product.id.toString()));

    NetworkInfo.checkConnectivity(context);

    return Consumer<ProductDetailsProvider>(
      builder: (context, details, child) {
        return Scaffold(
          appBar: CustomAppBar(title: 'Product Details'),
          bottomNavigationBar: BottomCartView(product: product),
          body: SingleChildScrollView(
            physics: BouncingScrollPhysics(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                ProductImageView(productModel: product),

                ProductTitleView(productModel: product),

                // Seller

                SellerView(sellerId: product.userId),

                // Specification
                Container(
                  margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                  color: Theme.of(context).accentColor,
                  child: ProductSpecification(
                      productSpecification: product.details ?? ''),
                ),

                // Related Products
                Container(
                  margin: EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                  color: Theme.of(context).accentColor,
                  child: Column(
                    children: [
                      TitleRow(title: 'Related Products', isDetailsPage: true),
                      SizedBox(height: 5),
                      RelatedProductView(categoryId: product.categoryId),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
