import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/body/order_place_model.dart';
import 'package:sendyshop/data/models/response/cart_model.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/helper/price_converter.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/provider/location_provider.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/amount_widget.dart';
import 'package:sendyshop/views/basewidget/animated_custom_dialog.dart';
import 'package:sendyshop/views/basewidget/custom_app_bar.dart';
import 'package:sendyshop/views/basewidget/image_display.dart';
import 'package:sendyshop/views/basewidget/my_dialog.dart';
import 'package:sendyshop/views/basewidget/title_row.dart';
import 'package:sendyshop/views/screens/cart/full_cart_screen.dart';
import 'package:sendyshop/views/screens/checkout/widget/custom_check_box.dart';
import 'package:sendyshop/views/screens/order/order_successful_screen.dart';

class CheckoutScreen extends StatefulWidget {
  static const routeName = '/checkoutscreen';
  final List<CartModel> cartList;
  CheckoutScreen({@required this.cartList});

  @override
  _CheckoutScreenState createState() => _CheckoutScreenState();
}

class _CheckoutScreenState extends State<CheckoutScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  double _order = 0;
  double _discount = 0;
  double _tax = 0;
  bool _isCashOnDeliveryActive;
  bool _isDigitalPaymentActive;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<LocationProvider>(context, listen: false)
            .initAddressList());

    WidgetsBinding.instance.addPostFrameCallback((_) =>
        Provider.of<OrderProvider>(context, listen: false).setShippingCost());

    _isCashOnDeliveryActive = true;
    _isDigitalPaymentActive = true;
    NetworkInfo.checkConnectivity(context);

    widget.cartList.forEach((cart) {
      double amount = cart.price;
      _order = _order + (amount * cart.quantity);
      _discount = _discount +
          PriceConverter.calculation(
              amount, cart.discount, cart.discountType, cart.quantity);
      _tax = _tax +
          PriceConverter.calculation(
              amount, cart.tax, cart.taxType, cart.quantity);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: Container(
        height: 60,
        padding: EdgeInsets.symmetric(
            horizontal: Dimensions.PADDING_SIZE_LARGE,
            vertical: Dimensions.PADDING_SIZE_DEFAULT),
        decoration: BoxDecoration(
            color: ColorResources.getPrimary(context),
            borderRadius: BorderRadius.only(
                topRight: Radius.circular(10), topLeft: Radius.circular(10))),
        child: Consumer<OrderProvider>(
          builder: (context, order, child) {
            double _shippingCost =
                Provider.of<LocationProvider>(context, listen: false)
                            .addressList
                            .length >
                        0
                    ? order.shippingCost
                    : 0;
            return Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    PriceConverter.convertPrice(
                        context, (_order + _shippingCost - _discount - _tax)),
                    style: rubikSemiBold.copyWith(
                        color: Theme.of(context).accentColor),
                  ),
                  !Provider.of<OrderProvider>(context).isLoading
                      ? Builder(
                          builder: (context) => ElevatedButton(
                            onPressed: () async {
                              if (Provider.of<LocationProvider>(context,
                                          listen: false)
                                      .addressList
                                      .length ==
                                  null) {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                        content:
                                            Text('Select a shipping address'),
                                        backgroundColor: Colors.red));
                              } else {
                                List<Cart> carts = [];
                                for (int index = 0;
                                    index < widget.cartList.length;
                                    index++) {
                                  CartModel cart = widget.cartList[index];
                                  carts.add(Cart(
                                    cart.id.toString(),
                                    PriceConverter.calculation(cart.price,
                                            cart.tax, cart.taxType, 1)
                                        .toInt(),
                                    cart.quantity,
                                    cart.price.toInt(),
                                    PriceConverter.calculation(cart.price,
                                            cart.discount, cart.discountType, 1)
                                        .toInt(),
                                    'amount',
                                    _shippingCost.toInt(),
                                  ));
                                }

                                order.placeOrder(
                                  OrderPlaceModel(
                                    cart: carts,
                                    paymentMethod: 'visa',
                                    deliveryAddressId:
                                        Provider.of<LocationProvider>(context,
                                                listen: false)
                                            .addressList[order.addressIndex]
                                            .id,
                                    orderAmount: double.parse(
                                        PriceConverter.convertPrice(
                                            context,
                                            (_order +
                                                _shippingCost -
                                                _discount -
                                                _tax))),
                                  ),
                                );
                                _callback(true, 'Order Placed Successful');
                              }
                            },
                            style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                            ),
                            child: Text('Place Order',
                                style: rubikSemiBold.copyWith(
                                  fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL,
                                  color: ColorResources.getPrimary(context),
                                )),
                          ),
                        )
                      : Container(
                          height: 30,
                          width: 100,
                          alignment: Alignment.center,
                          child: CircularProgressIndicator()),
                ]);
          },
        ),
      ),
      body: Consumer<LocationProvider>(builder: (context, address, child) {
        return Column(
          children: [
            CustomAppBar(title: 'checkout'),
            Expanded(
              child: ListView(
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.all(0),
                  children: [
                    // Shipping Details

                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: Dimensions.PADDING_SIZE_SMALL),
                      child: Row(children: [
                        Text('Delivery Address',
                            style: rubikMedium.copyWith(
                                fontSize: Dimensions.FONT_SIZE_LARGE)),
                        Expanded(child: SizedBox()),
                      ]),
                    ),
                    SizedBox(
                      height: 60,
                      child: address.addressList != null
                          ? address.addressList.length > 0
                              ? ListView.builder(
                                  shrinkWrap: true,
                                  physics: BouncingScrollPhysics(),
                                  scrollDirection: Axis.horizontal,
                                  padding: EdgeInsets.only(
                                      left: Dimensions.PADDING_SIZE_SMALL),
                                  itemCount: address.addressList.length,
                                  itemBuilder: (context, index) {
                                    return InkWell(
                                        onTap: () {
                                          Provider.of<LocationProvider>(context,
                                                  listen: false)
                                              .getAddress(
                                                  context,
                                                  address
                                                      .addressList[index].id);
                                        },
                                        child: Container(
                                          height: 60,
                                          width: 200,
                                          margin: EdgeInsets.only(
                                              right: Dimensions
                                                  .PADDING_SIZE_LARGE),
                                          decoration: BoxDecoration(
                                              color:
                                                  Theme.of(context).accentColor,
                                              borderRadius:
                                                  BorderRadius.circular(10),
                                              border: Border.all(
                                                  color:
                                                      ColorResources.getPrimary(
                                                          context),
                                                  width: 2)),
                                          child: Row(children: [
                                            Padding(
                                              padding: EdgeInsets.symmetric(
                                                  horizontal: Dimensions
                                                      .PADDING_SIZE_EXTRA_SMALL),
                                              child: Icon(
                                                address.addressList[index]
                                                            .addressType ==
                                                        'Home'
                                                    ? Icons.home_filled
                                                    : address.addressList[index]
                                                                .addressType ==
                                                            'Office'
                                                        ? Icons.work
                                                        : Icons
                                                            .list_alt_rounded,
                                                color:
                                                    ColorResources.getPrimary(
                                                        context),
                                                size: 30,
                                              ),
                                            ),
                                            Expanded(
                                              child: Column(
                                                  crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: [
                                                    Text(
                                                        address
                                                            .addressList[index]
                                                            .addressType,
                                                        style: rubikRegular
                                                            .copyWith(
                                                          fontSize: Dimensions
                                                              .FONT_SIZE_SMALL,
                                                          color: ColorResources
                                                              .getGreyBunkerColor(
                                                                  context),
                                                        )),
                                                    Text(
                                                        address
                                                            .addressList[index]
                                                            .address,
                                                        style: rubikRegular,
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis),
                                                  ]),
                                            ),
                                            Align(
                                              alignment: Alignment.topRight,
                                              child: Icon(Icons.check_circle,
                                                  color:
                                                      ColorResources.getPrimary(
                                                          context)),
                                            )
                                          ]),
                                        ));
                                  },
                                )
                              : Center(child: Text('No address available'))
                          : Center(
                              child: CircularProgressIndicator(
                                  valueColor: AlwaysStoppedAnimation<Color>(
                                      Theme.of(context).primaryColor))),
                    ),

                    // Order Details
                    Container(
                      margin:
                          EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                      color: Theme.of(context).accentColor,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            TitleRow(
                                title: 'Order Details',
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (_) => CartScreen(
                                              fromCheckout: true,
                                              checkoutCartList:
                                                  widget.cartList)));
                                }),
                            Padding(
                              padding:
                                  EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                              child: Row(children: [
                                Container(
                                    width: 50,
                                    height: 50,
                                    child: ImageWidget(
                                      url: widget.cartList[0].image,
                                    )),
                                SizedBox(width: Dimensions.MARGIN_SIZE_DEFAULT),
                                Expanded(
                                  flex: 3,
                                  child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          widget.cartList[0].name,
                                          style: rubikRegular.copyWith(
                                              fontSize: Dimensions
                                                  .FONT_SIZE_EXTRA_SMALL,
                                              color: ColorResources.getPrimary(
                                                  context)),
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        SizedBox(
                                            height: Dimensions
                                                .MARGIN_SIZE_EXTRA_SMALL),
                                        Row(children: [
                                          Text(
                                            PriceConverter.convertPrice(context,
                                                widget.cartList[0].price),
                                            style: rubikSemiBold.copyWith(
                                                color:
                                                    ColorResources.getPrimary(
                                                        context)),
                                          ),
                                          SizedBox(
                                              width: Dimensions
                                                  .PADDING_SIZE_SMALL),
                                          Text(
                                              widget.cartList[0].quantity
                                                  .toString(),
                                              style: rubikSemiBold.copyWith(
                                                  color:
                                                      ColorResources.getPrimary(
                                                          context))),
                                          Container(
                                            height: 20,
                                            padding: EdgeInsets.symmetric(
                                                horizontal: Dimensions
                                                    .PADDING_SIZE_EXTRA_SMALL),
                                            margin: EdgeInsets.only(
                                                left: Dimensions
                                                    .MARGIN_SIZE_EXTRA_LARGE),
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(16),
                                                border: Border.all(
                                                    color: ColorResources
                                                        .getPrimary(context))),
                                            child: Text(
                                              PriceConverter
                                                  .percentageCalculation(
                                                      context,
                                                      widget.cartList[0].price
                                                          .toString(),
                                                      widget
                                                          .cartList[0].discount
                                                          .toString(),
                                                      widget.cartList[0]
                                                          .discountType),
                                              style: rubikRegular.copyWith(
                                                  fontSize: Dimensions
                                                      .FONT_SIZE_EXTRA_SMALL,
                                                  color:
                                                      ColorResources.getPrimary(
                                                          context)),
                                            ),
                                          ),
                                        ]),
                                      ]),
                                ),
                              ]),
                            ),
                          ]),
                    ),

                    // Total bill
                    Container(
                      margin:
                          EdgeInsets.only(top: Dimensions.PADDING_SIZE_SMALL),
                      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                      color: Theme.of(context).accentColor,
                      child: Consumer<OrderProvider>(
                        builder: (context, order, child) {
                          double _shippingCost = address.addressList.length > 0
                              ? order.shippingCost
                              : 0;

                          return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                TitleRow(title: 'Total'),
                                AmountWidget(
                                    title: 'Order',
                                    amount: PriceConverter.convertPrice(
                                        context, _order)),
                                AmountWidget(
                                    title: 'Shipping Fee',
                                    amount: PriceConverter.convertPrice(
                                        context, _shippingCost)),
                                AmountWidget(
                                    title: 'Discount',
                                    amount: PriceConverter.convertPrice(
                                        context, _discount)),
                                AmountWidget(
                                    title: 'Tax',
                                    amount: PriceConverter.convertPrice(
                                        context, _tax)),
                                Divider(
                                    height: 10,
                                    color: Theme.of(context).hintColor),
                                AmountWidget(
                                    title: 'Total Payable',
                                    amount: PriceConverter.convertPrice(
                                        context,
                                        (_order +
                                            _shippingCost -
                                            _discount -
                                            _tax))),
                              ]);
                        },
                      ),
                    ),

                    // Payment Method
                    Padding(
                      padding: EdgeInsets.only(
                          top: Dimensions.PADDING_SIZE_SMALL,
                          left: Dimensions.PADDING_SIZE_SMALL),
                      child: Text('Payment Method',
                          style: rubikMedium.copyWith(
                              fontSize: Dimensions.FONT_SIZE_LARGE)),
                    ),
                    _isCashOnDeliveryActive
                        ? CustomCheckBox(title: 'Cash on delivery', index: 0)
                        : SizedBox(),
                    _isDigitalPaymentActive
                        ? CustomCheckBox(
                            title: 'Digital payment',
                            index: _isCashOnDeliveryActive ? 1 : 0)
                        : SizedBox(),
                    SizedBox(height: 20),
                  ]),
            ),
          ],
        );
      }),
    );
  }

  void _callback(bool isSuccess, String message) async {
    if (isSuccess) {
      Provider.of<CartProvider>(context, listen: false).clearCartList();
      Provider.of<OrderProvider>(context, listen: false).stopLoader();
      showAnimatedDialog(
          context,
          MyDialog(
            icon: Icons.done,
            title: 'Payment Done',
            description: 'Your Payment Successfully Done',
          ),
          dismissible: false,
          isFlip: true);
      Navigator.pushReplacement(context,
          MaterialPageRoute(builder: (_) => OrderSuccessfulScreen(status: 0)));
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(message), backgroundColor: Colors.red));
    }
  }
}
