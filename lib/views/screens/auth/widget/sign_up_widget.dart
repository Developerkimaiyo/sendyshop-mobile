import 'package:flutter/material.dart';

import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/body/register_model.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/provider/auth_provider.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/textfield/custom_password_textfield.dart';
import 'package:sendyshop/views/basewidget/textfield/custom_text_field.dart';
import 'package:sendyshop/views/screens/main/bottom_bar.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';

class SignUpWidget extends StatefulWidget {
  @override
  _SignUpWidgetState createState() => _SignUpWidgetState();
}

class _SignUpWidgetState extends State<SignUpWidget> {
  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  GlobalKey<FormState> _formKey;

  FocusNode _fNameFocus = FocusNode();
  FocusNode _lNameFocus = FocusNode();
  FocusNode _emailFocus = FocusNode();
  FocusNode _phoneFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();
  FocusNode _confirmPasswordFocus = FocusNode();

  RegisterModel registerBody = RegisterModel();
  bool isEmailVerified = false;

  addUser() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      isEmailVerified = true;

      if (_firstNameController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Name Is Required');
      } else if (_emailController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Email Is Required');
      } else if (_phoneController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Phone Is Required');
      } else if (_passwordController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Password Is Required');
      } else if (_passwordController.text.length < 6) {
        Ui.errorSnackBar(context, message: 'Password should be getter than 6');
      } else if (_confirmPasswordController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Confirm Password Is Required');
      } else if (_passwordController.text != _confirmPasswordController.text) {
        Ui.errorSnackBar(context, message: 'Password did not much');
      } else {
        registerBody.fName = _firstNameController.text;
        registerBody.lName = _lastNameController.text ?? " ";
        registerBody.email = _emailController.text;
        registerBody.phone = _phoneController.text;
        registerBody.password = _passwordController.text;
        await Provider.of<AuthProvider>(context, listen: false)
            .registration(context, registerBody);

        _emailController.clear();
        _passwordController.clear();
        _firstNameController.clear();
        _lastNameController.clear();
        _phoneController.clear();
        _confirmPasswordController.clear();
      }
    } else {
      isEmailVerified = false;
    }
  }

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey<FormState>();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL),
      children: [
        Form(
          key: _formKey,
          child: Column(
            children: [
              // for first and last name
              Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_DEFAULT,
                    right: Dimensions.MARGIN_SIZE_DEFAULT),
                child: Row(
                  children: [
                    Expanded(
                        child: CustomTextField(
                      hintText: 'First Name',
                      inputType: TextInputType.name,
                      focusNode: _fNameFocus,
                      nextFocus: _lNameFocus,
                      controller: _firstNameController,
                    )),
                    SizedBox(width: 15),
                    Expanded(
                        child: CustomTextField(
                      hintText: 'Last Name',
                      focusNode: _lNameFocus,
                      nextFocus: _emailFocus,
                      controller: _lastNameController,
                    )),
                  ],
                ),
              ),

              // for email
              Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_DEFAULT,
                    right: Dimensions.MARGIN_SIZE_DEFAULT,
                    top: Dimensions.MARGIN_SIZE_SMALL),
                child: CustomTextField(
                  hintText: 'Enter Your Email',
                  focusNode: _emailFocus,
                  nextFocus: _phoneFocus,
                  inputType: TextInputType.emailAddress,
                  controller: _emailController,
                ),
              ),

              // for phone

              Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_DEFAULT,
                    right: Dimensions.MARGIN_SIZE_DEFAULT,
                    top: Dimensions.MARGIN_SIZE_SMALL),
                child: CustomTextField(
                  inputType: TextInputType.number,
                  hintText: 'Enter Yor Mobile Number',
                  focusNode: _phoneFocus,
                  nextFocus: _passwordFocus,
                  controller: _phoneController,
                ),
              ),

              // for password
              Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_DEFAULT,
                    right: Dimensions.MARGIN_SIZE_DEFAULT,
                    top: Dimensions.MARGIN_SIZE_SMALL),
                child: CustomPasswordTextField(
                  hintTxt: 'Password',
                  controller: _passwordController,
                  focusNode: _passwordFocus,
                  nextNode: _confirmPasswordFocus,
                  textInputAction: TextInputAction.next,
                ),
              ),

              // for re-enter password
              Container(
                margin: EdgeInsets.only(
                    left: Dimensions.MARGIN_SIZE_DEFAULT,
                    right: Dimensions.MARGIN_SIZE_DEFAULT,
                    top: Dimensions.MARGIN_SIZE_SMALL),
                child: CustomPasswordTextField(
                  hintTxt: 'Re Enter Your Password',
                  controller: _confirmPasswordController,
                  focusNode: _confirmPasswordFocus,
                  textInputAction: TextInputAction.done,
                ),
              ),
            ],
          ),
        ),

        // for register button
        Container(
          margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 40),
          child: CustomButton(onTap: addUser, buttonText: 'Sign Up'),
        ),

        // for skip for now
        Center(
            child: TextButton(
          onPressed: () {
            Navigator.pushReplacement(
                context, MaterialPageRoute(builder: (_) => BottomBarScreen()));
          },
          child: Text('Skip For Now',
              style: rubikRegular.copyWith(
                  fontSize: Dimensions.FONT_SIZE_SMALL,
                  color: ColorResources.getPrimary(context))),
        )),
      ],
    );
  }
}
