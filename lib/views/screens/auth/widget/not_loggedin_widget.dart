import 'package:flutter/material.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/image_display.dart';
import 'package:sendyshop/views/screens/auth/auth_screen.dart';

class NotLoggedInWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(Dimensions.MARGIN_SIZE_EXTRA_LARGE),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
                height: MediaQuery.of(context).size.width * 0.4,
                width: MediaQuery.of(context).size.width,
                child: ImageWidget(
                  url: Images.login,
                )),
            SizedBox(height: 50),
            Text('Please Login First',
                textAlign: TextAlign.center, style: rubikSemiBold),
            SizedBox(height: 50),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: Dimensions.PADDING_SIZE_LARGE),
              child: CustomButton(
                buttonText: 'Login',
                onTap: () => Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => AuthScreen()),
                    (route) => false),
              ),
            ),
            InkWell(
              onTap: () {
                // Provider.of<AuthProvider>(context, listen: false).updateSelectedIndex(1);
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(builder: (context) => AuthScreen()),
                    (route) => false);
              },
              child: Padding(
                padding: EdgeInsets.symmetric(
                    vertical: Dimensions.PADDING_SIZE_LARGE, horizontal: 30),
                child: Text('Create New Account',
                    style: rubikRegular.copyWith(
                      color: Theme.of(context).primaryColor,
                      fontSize: Dimensions.FONT_SIZE_SMALL,
                    )),
              ),
            ),
          ],
        ));
  }
}
