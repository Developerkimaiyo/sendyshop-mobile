import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/body/login_model.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/provider/auth_provider.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/textfield/custom_password_textfield.dart';
import 'package:sendyshop/views/basewidget/textfield/custom_text_field.dart';
import 'package:sendyshop/views/screens/auth/forget_password_screen.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/styles.dart';

class SignInWidget extends StatefulWidget {
  @override
  _SignInWidgetState createState() => _SignInWidgetState();
}

class _SignInWidgetState extends State<SignInWidget> {
  TextEditingController _emailController;

  TextEditingController _passwordController;

  GlobalKey<FormState> _formKeyLogin;

  @override
  void initState() {
    super.initState();
    _formKeyLogin = GlobalKey<FormState>();

    _emailController = TextEditingController();
    _passwordController = TextEditingController();

    _emailController.text =
        Provider.of<AuthProvider>(context, listen: false).getUserEmail() ??
            null;
    _passwordController.text =
        Provider.of<AuthProvider>(context, listen: false).getUserPassword() ??
            null;
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  FocusNode _emailNode = FocusNode();
  FocusNode _passNode = FocusNode();
  LoginModel loginBody = LoginModel();

  void loginUser() async {
    if (_formKeyLogin.currentState.validate()) {
      _formKeyLogin.currentState.save();

      if (_emailController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Email Is Required');
      } else if (_passwordController.text.isEmpty) {
        Ui.errorSnackBar(context, message: 'Password Is Required');
      } else if (_passwordController.text.length < 6) {
        Ui.errorSnackBar(context, message: 'Password should be getter than 6');
      } else {
        if (Provider.of<AuthProvider>(context, listen: false).isRemember) {
          Provider.of<AuthProvider>(context, listen: false)
              .saveUserEmail(_emailController.text, _passwordController.text);
        } else {
          Provider.of<AuthProvider>(context, listen: false)
              .clearUserEmailAndPassword();
        }

        loginBody.email = _emailController.text;
        loginBody.password = _passwordController.text;
        await Provider.of<AuthProvider>(context, listen: false)
            .login(context, loginBody);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<AuthProvider>(context, listen: false).isRemember;
    return Form(
      key: _formKeyLogin,
      child: ListView(
        padding: EdgeInsets.symmetric(vertical: Dimensions.PADDING_SIZE_SMALL),
        children: [
          // for Email
          Container(
              margin: EdgeInsets.only(
                  left: Dimensions.MARGIN_SIZE_LARGE,
                  right: Dimensions.MARGIN_SIZE_LARGE,
                  bottom: Dimensions.MARGIN_SIZE_SMALL),
              child: CustomTextField(
                hintText: 'Enter Your Email',
                focusNode: _emailNode,
                nextFocus: _passNode,
                inputType: TextInputType.emailAddress,
                controller: _emailController,
              )),

          // for Password
          Container(
              margin: EdgeInsets.only(
                  left: Dimensions.MARGIN_SIZE_LARGE,
                  right: Dimensions.MARGIN_SIZE_LARGE,
                  bottom: Dimensions.MARGIN_SIZE_DEFAULT),
              child: CustomPasswordTextField(
                hintTxt: 'Enter Your Password',
                textInputAction: TextInputAction.done,
                focusNode: _passNode,
                controller: _passwordController,
              )),

          // for remember and forgetpassword
          Container(
            margin: EdgeInsets.only(
                left: Dimensions.MARGIN_SIZE_SMALL,
                right: Dimensions.MARGIN_SIZE_SMALL),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Consumer<AuthProvider>(
                      builder: (context, authProvider, child) => Checkbox(
                        checkColor: ColorResources.COLOR_WHITE,
                        activeColor: ColorResources.COLOR_PRIMARY,
                        value: authProvider.isRemember,
                        onChanged: authProvider.updateRemember,
                      ),
                    ),
                    //

                    Text('Remember', style: rubikRegular),
                  ],
                ),
                InkWell(
                  onTap: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) => ForgetPasswordScreen())),
                  child: Text('Forgot Password',
                      style: rubikRegular.copyWith(
                          color: ColorResources.getPrimary(context))),
                ),
              ],
            ),
          ),

          // for signin button
          Container(
            margin: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 30),
            child: CustomButton(onTap: loginUser, buttonText: 'Sign In'),
          ),
        ],
      ),
    );
  }
}
