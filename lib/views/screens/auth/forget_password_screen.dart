import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/body/forgot_password_model.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/auth_provider.dart';
import 'package:sendyshop/util/color_resources.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';
import 'package:sendyshop/views/basewidget/animated_custom_dialog.dart';
import 'package:sendyshop/views/basewidget/button/custom_button.dart';
import 'package:sendyshop/views/basewidget/my_dialog.dart';
import 'package:sendyshop/views/basewidget/textfield/custom_text_field.dart';

class ForgetPasswordScreen extends StatefulWidget {
  @override
  _ForgetPasswordScreenState createState() => _ForgetPasswordScreenState();
}

class _ForgetPasswordScreenState extends State<ForgetPasswordScreen> {
  TextEditingController _emailController;
  GlobalKey<FormState> _formKeyForgetPass;

  @override
  void initState() {
    super.initState();
    _formKeyForgetPass = GlobalKey<FormState>();

    _emailController = TextEditingController();

    _emailController.text =
        Provider.of<AuthProvider>(context, listen: false).getUserEmail() ??
            null;
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  FocusNode _emailNode = FocusNode();
  ForgotPassModel forgotPassBody = ForgotPassModel();

  @override
  Widget build(BuildContext context) {
    NetworkInfo.checkConnectivity(context);
    _formKeyForgetPass = GlobalKey<FormState>();
    _emailController = TextEditingController();

    _emailController.text =
        Provider.of<AuthProvider>(context, listen: false).getUserEmail() ??
            null;
    return Scaffold(
      key: _formKeyForgetPass,
      body: Container(
        decoration: BoxDecoration(
          image: Provider.of<ThemeProvider>(context).darkTheme
              ? null
              : DecorationImage(
                  image: AssetImage(Images.background), fit: BoxFit.fill),
        ),
        child: Column(
          children: [
            SafeArea(
                child: Align(
              alignment: Alignment.centerLeft,
              child: IconButton(
                icon: Icon(Icons.arrow_back_ios_outlined),
                onPressed: () => Navigator.pop(context),
              ),
            )),
            Expanded(
              child: ListView(
                  padding: EdgeInsets.all(Dimensions.PADDING_SIZE_SMALL),
                  children: [
                    Padding(
                      padding: EdgeInsets.all(50),
                      child: Image.asset(Images.logo,
                          height: 150,
                          width: 200,
                          color: ColorResources.getPrimary(context)),
                    ),
                    Text('Forgot Password', style: rubikSemiBold),
                    Row(children: [
                      Expanded(
                          flex: 1,
                          child: Divider(
                              thickness: 1,
                              color: ColorResources.COLOR_PRIMARY)),
                      Expanded(
                          flex: 2,
                          child: Divider(
                              thickness: 0.2,
                              color: ColorResources.COLOR_PRIMARY)),
                    ]),
                    Text('Enter Email For Password Reset',
                        style: rubikRegular.copyWith(
                            color: Theme.of(context).hintColor,
                            fontSize: Dimensions.FONT_SIZE_EXTRA_SMALL)),
                    SizedBox(height: Dimensions.PADDING_SIZE_LARGE),
                    CustomTextField(
                      controller: _emailController,
                      hintText: 'Enter You Email',
                      focusNode: _emailNode,
                      inputAction: TextInputAction.done,
                      inputType: TextInputType.emailAddress,
                    ),
                    SizedBox(height: 100),
                    Builder(
                      builder: (context) => CustomButton(
                          buttonText: 'Send Email',
                          onTap: () {
                            if (_emailController.text.isEmpty) {
                              Ui.errorSnackBar(context,
                                  message: 'Email Is Required');
                            } else {
                              forgotPassBody.email = _emailController.text;

                              Provider.of<AuthProvider>(context, listen: false)
                                  .ForgotPass(context, forgotPassBody);
                            }
                          }),
                    ),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}
