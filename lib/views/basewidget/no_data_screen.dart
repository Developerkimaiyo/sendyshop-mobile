import 'package:flutter/material.dart';
import 'package:sendyshop/util/dimensions.dart';
import 'package:sendyshop/util/images.dart';
import 'package:sendyshop/util/styles.dart';

class NoDataScreen extends StatelessWidget {
  final bool isOrder;
  final bool isCart;
  final bool isNothing;
  NoDataScreen(
      {this.isCart = false, this.isNothing = false, this.isOrder = false});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Dimensions.PADDING_SIZE_LARGE),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Image.asset(
                isOrder
                    ? Images.clock
                    : isCart
                        ? Images.cart_image
                        : Images.binoculars,
                width: 150,
                height: 150,
                color: Theme.of(context).primaryColor),
            SizedBox(height: 30),
            Text(
              isOrder
                  ? 'no_order_history_available'
                  : isCart
                      ? 'empty_cart'
                      : 'nothing_found',
              style: rubikBold.copyWith(
                  color: Theme.of(context).primaryColor, fontSize: 18),
              textAlign: TextAlign.center,
            ),
            SizedBox(height: 10),
            Text(
              isOrder
                  ? 'buy_something_to_see'
                  : isCart
                      ? 'look_like_have_not_added'
                      : '',
              style: rubikMedium,
              textAlign: TextAlign.center,
            ),
          ]),
    );
  }
}
