import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:sendyshop/util/images.dart';

class ImageWidget extends StatelessWidget {
  final String url;

  ImageWidget({@required this.url});

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      placeholder: (context, url) => Image.asset(Images.placeholder_image),
      errorWidget: (context, url, error) =>
          Image.asset(Images.placeholder_image),
    );
  }
}
