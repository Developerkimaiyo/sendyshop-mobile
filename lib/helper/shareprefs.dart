import 'package:shared_preferences/shared_preferences.dart';

class SharedPrefs {
  static SharedPreferences sharedPreferences;

  static init() async {
    if (sharedPreferences == null) {
      sharedPreferences = await SharedPreferences.getInstance();
    }
  }
}
