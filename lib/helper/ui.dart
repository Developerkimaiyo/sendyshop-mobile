import 'package:flutter/material.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:sendyshop/util/color_resources.dart';

class Ui {
  static Color colorFromHex(String hexColor) {
    final hexCode = hexColor.replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return Color(int.parse('FF$hexCode', radix: 16));
  }

  static successSnackBar(BuildContext context,
      {String title = 'Success', String message}) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .headline6
            .merge(TextStyle(color: ColorResources.COLOR_WHITE)),
      ),
      messageText: Text(
        message,
        style: Theme.of(context)
            .textTheme
            .caption
            .merge(TextStyle(color: ColorResources.COLOR_WHITE)),
      ),
      flushbarPosition: FlushbarPosition.BOTTOM,
      margin: EdgeInsets.all(20.0),
      backgroundColor: Colors.green,
      icon: Icon(
        Icons.check_circle_outline,
        size: 32.0,
        color: ColorResources.COLOR_WHITE,
      ),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 18.0),
      borderRadius: BorderRadius.circular(8),
      duration: Duration(seconds: 5),
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
    ).show(context);
  }

  static errorSnackBar(BuildContext context,
      {String title = 'Error', String message}) {
    Flushbar(
      titleText: Text(
        title,
        style: Theme.of(context)
            .textTheme
            .headline6
            .merge(TextStyle(color: ColorResources.COLOR_WHITE)),
      ),
      messageText: Text(
        message,
        style: Theme.of(context)
            .textTheme
            .caption
            .merge(TextStyle(color: ColorResources.COLOR_WHITE)),
      ),
      flushbarPosition: FlushbarPosition.BOTTOM,
      margin: EdgeInsets.all(20.0),
      backgroundColor: Colors.red,
      icon: Icon(
        Icons.remove_circle_outline,
        size: 32.0,
        color: ColorResources.COLOR_WHITE,
      ),
      padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 18.0),
      borderRadius: BorderRadius.circular(8),
      dismissDirection: FlushbarDismissDirection.HORIZONTAL,
      duration: Duration(seconds: 5),
    ).show(context);
  }
}
