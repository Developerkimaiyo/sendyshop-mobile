import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/repository/auth_repo.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/provider/profile_provider.dart';
import 'package:sendyshop/views/basewidget/animated_custom_dialog.dart';
import 'package:sendyshop/views/basewidget/my_dialog.dart';
import 'package:sendyshop/views/screens/auth/auth_screen.dart';
import 'package:sendyshop/views/screens/main/bottom_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthProvider with ChangeNotifier {
  final AuthRepo authRepo;
  final SharedPreferences sharedPreferences;
  AuthProvider({@required this.sharedPreferences, @required this.authRepo});
  bool _isRemember = false;
  bool isLoading = false;
  int _selectedIndex = 0;
  int get selectedIndex => _selectedIndex;

  updateSelectedIndex(int index) {
    _selectedIndex = index;
    notifyListeners();
  }

  bool get isRemember => _isRemember;

  void updateRemember(bool value) {
    _isRemember = value;
    notifyListeners();
  }

  Future<void> registration(BuildContext context, data) async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await authRepo.instanceSignUp(data);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      notifyListeners();
      authRepo.saveUserToken(apiResponse.response.data['token']);

      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);

      Future.delayed(Duration(seconds: 2), () {
        Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context);
      });
    } else {
      isLoading = false;
      notifyListeners();
      Ui.errorSnackBar(context,
          message: apiResponse.response.data["message"].toString());
    }
  }

  Future<void> login(BuildContext context, data) async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await authRepo.instanceLogin(data);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      notifyListeners();
      authRepo.saveUserToken(apiResponse.response.data['token']);

      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);

      Future.delayed(Duration(seconds: 2), () {
        Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context);
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => BottomBarScreen()));
      });
    } else {
      isLoading = false;
      notifyListeners();
      Ui.errorSnackBar(context, message: apiResponse.response.data["message"]);
    }
  }

  Future<void> ForgotPass(BuildContext context, data) async {
    notifyListeners();
    ApiResponse apiResponse = await authRepo.instanceForgotPass(data);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      notifyListeners();
      showAnimatedDialog(
          context,
          MyDialog(
            icon: Icons.send,
            title: apiResponse.response.data["message"],
            description: 'Recovery Link Sent',
            rotateAngle: 5.5,
          ),
          dismissible: false);
      Future.delayed(Duration(seconds: 2), () {
        Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context);
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => AuthScreen()));
      });
    } else {
      notifyListeners();
      Ui.errorSnackBar(context, message: apiResponse.response.data["message"]);
    }
  }

  // for user Section
  String getUserToken() {
    return authRepo.getUserToken();
  }

  bool isLoggedIn() {
    return authRepo.isLoggedIn();
  }

  Future<bool> clearSharedData(context) async {
    return await authRepo.clearSharedData();
  }

  // for  Remember Email
  void saveUserEmail(String email, String password) {
    authRepo.saveUserEmailAndPassword(email, password);
  }

  String getUserEmail() {
    return authRepo.getUserEmail() ?? "";
  }

  Future<bool> clearUserEmailAndPassword() async {
    return authRepo.clearUserEmailAndPassword();
  }

  String getUserPassword() {
    return authRepo.getUserPassword() ?? "";
  }
}
