import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/body/order_place_model.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/order_details.dart';
import 'package:sendyshop/data/models/response/response_model.dart';
import 'package:sendyshop/data/models/response/order_model.dart';
import 'package:sendyshop/data/repository/order_repo.dart';
import 'package:sendyshop/helper/ui.dart';

class OrderProvider with ChangeNotifier {
  final OrderRepo orderRepo;

  OrderProvider({@required this.orderRepo});
  List<OrderModel> OrderList = [];
  List<OrderModel> processingOrderList = [];
  List<OrderModel> transitOrderList = [];
  List<OrderModel> deliveredOrderList = [];
  List<OrderModel> cancelOrderList = [];

  List<OrderDetailsModel> _orderDetails;
  int _paymentMethodIndex = 0;
  OrderModel _trackModel;
  ResponseModel _responseModel;
  int _addressIndex = 0;
  bool _isLoading = false;
  double _shippingCost = 0.0;

  int get paymentMethodIndex => _paymentMethodIndex;
  int get addressIndex => _addressIndex;
  double get shippingCost => _shippingCost;
  List<OrderDetailsModel> get orderDetails => _orderDetails;
  OrderModel get trackModel => _trackModel;
  ResponseModel get responseModel => _responseModel;
  bool get isLoading => _isLoading;

  Future<void> getOrderList() async {
    _isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.getOrder();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _isLoading = false;
      notifyListeners();
      OrderList = List<OrderModel>.from(
          apiResponse.response.data.map((data) => OrderModel.fromJson(data)));
      notifyListeners();
    } else {
      _isLoading = false;
      notifyListeners();
    }
  }

  Future<void> getProcessingOrderList() async {
    _isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.getProcessingOrder();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _isLoading = false;
      notifyListeners();
      processingOrderList = List<OrderModel>.from(
          apiResponse.response.data.map((data) => OrderModel.fromJson(data)));
      notifyListeners();
    } else {
      _isLoading = false;
      notifyListeners();
    }
  }

  Future<void> getTransitOrderList() async {
    _isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.getTransitOrder();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _isLoading = false;
      notifyListeners();
      transitOrderList = List<OrderModel>.from(
          apiResponse.response.data.map((data) => OrderModel.fromJson(data)));
      notifyListeners();
    } else {
      _isLoading = false;
      notifyListeners();
    }
  }

  Future<void> getDeliveredOrder() async {
    _isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.getDeliveredOrder();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _isLoading = false;
      notifyListeners();
      deliveredOrderList = List<OrderModel>.from(
          apiResponse.response.data.map((data) => OrderModel.fromJson(data)));
      notifyListeners();
    } else {
      _isLoading = false;
      notifyListeners();
    }
  }

  Future<void> getCancelOrder() async {
    _isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.getCancelOrder();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _isLoading = false;
      notifyListeners();
      cancelOrderList = List<OrderModel>.from(
          apiResponse.response.data.map((data) => OrderModel.fromJson(data)));
      notifyListeners();
    } else {
      _isLoading = false;
      notifyListeners();
    }
  }

  Future<void> updateDataStatus(BuildContext context, data, id) async {
    _isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.updateStatus(data, id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _isLoading = false;
      notifyListeners();
      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);
      Provider.of<OrderProvider>(context, listen: false)
          .getProcessingOrderList();
      Provider.of<OrderProvider>(context, listen: false).getTransitOrderList();
      Provider.of<OrderProvider>(context, listen: false).getDeliveredOrder();
      Provider.of<OrderProvider>(context, listen: false).getCancelOrder();
    } else {
      _isLoading = false;
      notifyListeners();
      Ui.errorSnackBar(context,
          message: apiResponse.response.data["message"].toString());
    }
  }

  Future<void> deleteUserAddressByID(BuildContext context, int id) async {
    notifyListeners();
    ApiResponse apiResponse = await orderRepo.deleteOrder(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);
      Provider.of<OrderProvider>(context, listen: false).getCancelOrder();
      notifyListeners();
    } else {
      print(apiResponse.error.toString());
      notifyListeners();
    }
  }

  Future<List<OrderDetailsModel>> getOrderDetails(String orderID) async {
    _orderDetails = null;
    _isLoading = true;
    ApiResponse apiResponse = await orderRepo.getOrderDetails(orderID);
    _isLoading = false;
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _orderDetails = [];
      apiResponse.response.data
          .forEach((orderDetail) => _orderDetails.add(orderDetail));
    } else {
      print(apiResponse.error.errors[0].message);
    }
    notifyListeners();
    return _orderDetails;
  }

  void setPaymentMethod(int index) {
    _paymentMethodIndex = index;
    notifyListeners();
  }

  Future<ResponseModel> trackOrder(String orderID) async {
    _trackModel = null;
    _responseModel = null;
    ApiResponse apiResponse = await orderRepo.trackOrder(orderID);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      _trackModel = apiResponse.response.data;
      _responseModel =
          ResponseModel(true, apiResponse.response.data.toString());
    } else {
      _responseModel =
          ResponseModel(false, apiResponse.error.errors[0].message);
    }
    notifyListeners();
    return _responseModel;
  }

  Future<void> placeOrder(OrderPlaceModel placeOrderBody) async {
    _isLoading = true;
    notifyListeners();
    _isLoading = false;
    _addressIndex = 0;
    print('-------- Order placed successfully 100002 ----------');
    notifyListeners();
  }

  void stopLoader() {
    _isLoading = false;
    notifyListeners();
  }

  void setAddressIndex(int index) {
    _addressIndex = index;
    if (index != 0) {
      notifyListeners();
    }
  }

  void cancelOrder(String orderID, Function callback) async {
    _isLoading = true;
    _isLoading = false;
    OrderModel orderModel;
    OrderList.forEach((order) {
      if (order.id.toString() == orderID) {
        orderModel = order;
      }
    });
    OrderList.remove(orderModel);
    callback('Successful', true, orderID);
    notifyListeners();
  }

  void clearHistory(String orderID, Function callback) async {
    _isLoading = true;
    _isLoading = false;
    OrderModel orderModel;
    OrderList.forEach((order) {
      if (order.id.toString() == orderID) {
        orderModel = order;
      }
    });
    OrderList.remove(orderModel);
    callback('Successful', true, orderID);
    notifyListeners();
  }

  void setShippingCost() {
    _shippingCost = orderRepo.getShipingCost().shippingCost;
    notifyListeners();
  }
}
