import 'package:flutter/material.dart';
import 'package:sendyshop/data/repository/Theme_repo.dart';

class ThemeProvider with ChangeNotifier {
  final ThemeRepo themeRepo;

  ThemeProvider({@required this.themeRepo});
  bool _darkTheme = false;
  bool get darkTheme => _darkTheme;

  set darkTheme(bool value) {
    _darkTheme = value;
    themeRepo.setDarkTheme(value);
    notifyListeners();
  }
}
