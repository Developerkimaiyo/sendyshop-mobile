import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/user_model.dart';
import 'package:sendyshop/data/repository/profile_repo.dart';
import 'package:sendyshop/helper/ui.dart';

class ProfileProvider extends ChangeNotifier {
  final ProfileRepo profileRepo;
  ProfileProvider({@required this.profileRepo});
  UserModel userInfoModel;
  bool isAvailableProfile = false;
  bool isLoading = false;
  bool hasData;

  Future getUserInfo(BuildContext context) async {
    notifyListeners();
    ApiResponse apiResponse = await profileRepo.getUserInfo();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      notifyListeners();
      userInfoModel = UserModel.fromJson(apiResponse.response.data);
    } else {
      notifyListeners();
      Ui.errorSnackBar(context, message: apiResponse.response.data["message"]);
    }
  }

  Future<void> updateUserInfo(BuildContext context, data) async {
    notifyListeners();
    ApiResponse apiResponse = await profileRepo.upDateUser(data);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      notifyListeners();
      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);
      Future.delayed(Duration(seconds: 2), () {
        Provider.of<ProfileProvider>(context, listen: false)
            .getUserInfo(context);
      });
    } else {
      notifyListeners();
      Ui.errorSnackBar(context,
          message: apiResponse.response.data["message"].toString());
    }
  }
}
