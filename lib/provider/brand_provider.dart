import 'package:flutter/material.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/brand_model.dart';
import 'package:sendyshop/data/repository/brand_repo.dart';

class BrandProvider extends ChangeNotifier {
  final BrandRepo brandRepo;

  BrandProvider({@required this.brandRepo});

  List<BrandModel> _brandList = [];

  List<BrandModel> get brandList => _brandList;

  Future<void> initBrandList() async {
    _brandList.clear();
    notifyListeners();
    ApiResponse apiResponse = await brandRepo.getBrandList();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      notifyListeners();
      _brandList = List<BrandModel>.from(
          apiResponse.response.data.map((data) => BrandModel.fromJson(data)));
      notifyListeners();
    } else {
      notifyListeners();
    }
  }
}
