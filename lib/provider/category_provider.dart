import 'package:flutter/material.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/category_model.dart';
import 'package:sendyshop/data/repository/category_repo.dart';

class CategoryProvider extends ChangeNotifier {
  final CategoryRepo categoryRepo;

  CategoryProvider({@required this.categoryRepo});
  List<Category> _categoryList = [];

  List<Category> get categoryList => _categoryList;

  Future<void> initCategoryList() async {
    _categoryList.clear();
    notifyListeners();
    ApiResponse apiResponse = await categoryRepo.getCategoryList();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      notifyListeners();
      _categoryList = List<Category>.from(
          apiResponse.response.data.map((data) => Category.fromJson(data)));
      notifyListeners();
    } else {
      notifyListeners();
    }
  }
}
