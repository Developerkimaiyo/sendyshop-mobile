import 'package:flutter/material.dart';
import 'package:sendyshop/data/repository/product_details_repo.dart';

class ProductDetailsProvider extends ChangeNotifier {
  final ProductDetailsRepo productDetailsRepo;

  ProductDetailsProvider({@required this.productDetailsRepo});

  int _imageSliderIndex;
  bool _wish = false;
  int _quantity = 1;

  bool _isLoading = false;
  int _wishCount;
  String _sharableLink;

  int get imageSliderIndex => _imageSliderIndex;
  bool get isWished => _wish;
  int get quantity => _quantity;
  bool get isLoading => _isLoading;
  int get wishCount => _wishCount;
  String get sharableLink => _sharableLink;

  void getSharableLink(String productID) async {
    _sharableLink = productDetailsRepo.getSharableLink();
  }

  void setImageSliderSelectedIndex(int selectedIndex) {
    _imageSliderIndex = selectedIndex;
    notifyListeners();
  }

  void changeWish() {
    _wish = !_wish;
    notifyListeners();
  }

  void setQuantity(int value) {
    _quantity = value;
    notifyListeners();
  }
}
