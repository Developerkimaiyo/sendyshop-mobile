import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sendyshop/data/models/response/address_model.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/repository/location_repo.dart';
import 'package:sendyshop/helper/ui.dart';
import 'package:sendyshop/views/screens/address/add_new_address_screen.dart';

import 'package:shared_preferences/shared_preferences.dart';

class LocationProvider with ChangeNotifier {
  final SharedPreferences sharedPreferences;
  final LocationRepo locationRepo;
  LocationProvider({@required this.sharedPreferences, this.locationRepo});
  AddressModel addressModel;
  bool isLoading = false;
  // delete user address
  void deleteUserAddressByID(BuildContext context, int id) async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await locationRepo.deleteAddress(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);
      Provider.of<LocationProvider>(context, listen: false).initAddressList();
      notifyListeners();
    } else {
      isLoading = false;
      print(apiResponse.error.toString());
      notifyListeners();
    }
  }

  Future getAddress(BuildContext context, int addressId) async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await locationRepo.getAddress(addressId);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      notifyListeners();
      addressModel = AddressModel.fromJson(apiResponse.response.data);

      Future.delayed(Duration(seconds: 2), () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => AddNewAddressScreen(isEnableUpdate: true)));
      });
    } else {
      isLoading = false;
      notifyListeners();
      Ui.errorSnackBar(context, message: apiResponse.response.data["message"]);
    }
  }

  bool isAvaibleLocation = false;

  // user address
  List<AddressModel> addressList = [];

  void initAddressList() async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await locationRepo.getAllAddress();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      notifyListeners();
      addressList = List<AddressModel>.from(apiResponse.response.data
          .map((address) => AddressModel.fromJson(address)));
      notifyListeners();
    } else {
      isLoading = false;
      print(apiResponse.error.toString());
      notifyListeners();
    }
  }

  // for address update screen

  Future<void> updateAddress(BuildContext context, data, id) async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await locationRepo.upDataAddress(data, id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      notifyListeners();
      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);
      Future.delayed(Duration(seconds: 1), () {
        Provider.of<LocationProvider>(context, listen: false).initAddressList();
      });
    } else {
      isLoading = false;
      notifyListeners();
      Ui.errorSnackBar(context, message: apiResponse.response.data["message"]);
    }
  }

  Future<void> addAddress(BuildContext context, data) async {
    isLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await locationRepo.createAddress(data);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      isLoading = false;
      notifyListeners();

      Ui.successSnackBar(context,
          message: apiResponse.response.data["message"]);
      Future.delayed(Duration(seconds: 1), () {
        Provider.of<LocationProvider>(context, listen: false).initAddressList();
      });
    } else {
      isLoading = false;
      notifyListeners();
      Ui.errorSnackBar(context,
          message: apiResponse.response.data["message"].toString());
    }
  }

  // for Label Us
  List<String> getAllAddressType = [];
  int selectAddressIndex = 0;
  String addressType = '';

  updateAddressIndex(int index) {
    selectAddressIndex = index;
    addressType = getAllAddressType[index];
    notifyListeners();
  }
}
