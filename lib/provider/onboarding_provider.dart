import 'package:flutter/foundation.dart';
import 'package:sendyshop/data/models/response/onboarding_model.dart';
import 'package:sendyshop/data/repository/onboarding_repo.dart';

class OnboardingProvider with ChangeNotifier {
  final OnboardingRepo onboardingRepo;

  OnboardingProvider({@required this.onboardingRepo});
  List<OnBoardingModel> _onBoardingList = [];

  List<OnBoardingModel> get onBoardingList => _onBoardingList;

  int _selectedIndex = 0;
  int get selectedIndex => _selectedIndex;
  set selectedIndex(int value) {
    _selectedIndex = value;
    notifyListeners();
  }

  changeOnboardingIndex(int index) {
    _selectedIndex = index;
    notifyListeners();
  }

  void initializeBoardingList() {
    if (_onBoardingList.length == 0) {
      _onBoardingList.clear();
      _onBoardingList = onboardingRepo.getOnboardingList;
      notifyListeners();
    }
  }
}
