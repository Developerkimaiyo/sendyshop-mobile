import 'package:flutter/material.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/data/repository/product_repo.dart';

class ProductProvider extends ChangeNotifier {
  final ProductRepo productRepo;

  ProductProvider({@required this.productRepo});
  // Latest products
  List<Product> latestProductList = [];
  bool firstLoading = false;

  Future<void> initLatestProductList() async {
    firstLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await productRepo.getLatestProductList();
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      firstLoading = false;
      notifyListeners();
      latestProductList = List<Product>.from(
          apiResponse.response.data.map((data) => Product.fromJson(data)));
      notifyListeners();
    } else {
      firstLoading = false;
      print(apiResponse.error.toString());
      notifyListeners();
    }
  }

  void removeFirstLoading() {
    firstLoading = true;
    notifyListeners();
  }

  // Seller products
  List<Product> sellerAllProductList = [];
  List<Product> sellerProductList = [];

  Future<void> initSellerProductList(BuildContext context, String id) async {
    firstLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await productRepo.getSellerProductList(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      firstLoading = false;
      notifyListeners();
      sellerProductList = List<Product>.from(
          apiResponse.response.data.map((data) => Product.fromJson(data)));
      notifyListeners();
    } else {
      firstLoading = false;
      print(apiResponse.error.toString());
      notifyListeners();
    }
  }

  void filterData(String newText) {
    sellerProductList.clear();
    if (newText.isNotEmpty) {
      sellerAllProductList.forEach((product) {
        if (product.name.toLowerCase().contains(newText.toLowerCase())) {
          sellerProductList.add(product);
        }
      });
    } else {
      sellerProductList.clear();
      sellerProductList.addAll(sellerAllProductList);
    }
    notifyListeners();
  }

  void clearSellerData() {
    sellerProductList = [];
    notifyListeners();
  }

  List<Product> brandProductList = [];
  Future<void> initBrandProductList(BuildContext context, String id) async {
    firstLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await productRepo.getBrandProductList(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      firstLoading = false;
      notifyListeners();
      brandProductList = List<Product>.from(
          apiResponse.response.data.map((data) => Product.fromJson(data)));
      notifyListeners();
    } else {
      firstLoading = false;
      notifyListeners();
    }
  }

  // Brand and category products
  List<Product> categoryProductList = [];

  Future<void> initCategoryProductList(BuildContext context, String id) async {
    categoryProductList.clear();
    firstLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await productRepo.getCategoryProductList(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      firstLoading = false;
      notifyListeners();
      categoryProductList = List<Product>.from(
          apiResponse.response.data.map((data) => Product.fromJson(data)));
      notifyListeners();
    } else {
      firstLoading = false;
      notifyListeners();
    }
  }

  // Related products
  List<Product> relatedProductList = [];

  Future<void> initRelatedProductList(BuildContext context, String id) async {
    firstLoading = true;
    notifyListeners();
    ApiResponse apiResponse = await productRepo.getRelatedProductList(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      firstLoading = false;
      notifyListeners();
      relatedProductList = List<Product>.from(
          apiResponse.response.data.map((data) => Product.fromJson(data)));
      notifyListeners();
    } else {
      firstLoading = false;
      print(apiResponse.error.toString());
      notifyListeners();
    }
  }

  void removePrevRelatedProduct() {
    relatedProductList = null;
    notifyListeners();
  }
}
