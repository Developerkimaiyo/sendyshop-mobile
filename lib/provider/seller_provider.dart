import 'package:flutter/material.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/seller_model.dart';
import 'package:sendyshop/data/repository/seller_repo.dart';
import 'package:sendyshop/helper/ui.dart';

class SellerProvider extends ChangeNotifier {
  final SellerRepo sellerRepo;
  SellerProvider({@required this.sellerRepo});

  List<SellerModel> sellerList;
  List<SellerModel> sellerAllList;
  List<SellerModel> orderSellerList = [];
  SellerModel sellerModel;
  bool isSearching = false;

  Future initSeller(BuildContext context, String id) async {
    notifyListeners();
    ApiResponse apiResponse = await sellerRepo.getSeller(id);
    if (apiResponse.response != null &&
        apiResponse.response.statusCode == 200) {
      notifyListeners();
      sellerModel = SellerModel.fromJson(apiResponse.response.data);
    } else {
      Ui.errorSnackBar(context, message: apiResponse.response.data["message"]);
    }
    notifyListeners();
  }

  void toggleSearch() {
    isSearching = !isSearching;
    notifyListeners();
  }

  void filterList(String query) {
    sellerList.clear();
    if (query.isNotEmpty) {
      sellerAllList.forEach((seller) {
        if ((seller.fName + ' ' + seller.lName)
            .toLowerCase()
            .contains(query.toLowerCase())) {
          sellerList.add(seller);
        }
      });
    } else {
      sellerList.addAll(sellerAllList);
    }
    notifyListeners();
  }

  void removePrevOrderSeller() {
    orderSellerList = [];
    notifyListeners();
  }
}
