import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/dio/logging_interceptor.dart';
import 'package:sendyshop/data/repository/Theme_repo.dart';
import 'package:sendyshop/data/repository/auth_repo.dart';
import 'package:sendyshop/data/repository/brand_repo.dart';
import 'package:sendyshop/data/repository/cart_repo.dart';
import 'package:sendyshop/data/repository/category_repo.dart';
import 'package:sendyshop/data/repository/location_repo.dart';
import 'package:sendyshop/data/repository/onboarding_repo.dart';
import 'package:sendyshop/data/repository/order_repo.dart';
import 'package:sendyshop/data/repository/product_details_repo.dart';
import 'package:sendyshop/data/repository/product_repo.dart';
import 'package:sendyshop/data/repository/profile_repo.dart';
import 'package:sendyshop/data/repository/search_repo.dart';
import 'package:sendyshop/data/repository/seller_repo.dart';
import 'package:sendyshop/helper/network_info.dart';
import 'package:sendyshop/provider/Theme_provider.dart';
import 'package:sendyshop/provider/auth_provider.dart';
import 'package:sendyshop/provider/brand_provider.dart';
import 'package:sendyshop/provider/cart_provider.dart';
import 'package:sendyshop/provider/category_provider.dart';
import 'package:sendyshop/provider/location_provider.dart';
import 'package:sendyshop/provider/onboarding_provider.dart';
import 'package:sendyshop/provider/order_provider.dart';
import 'package:sendyshop/provider/product_details_provider.dart';
import 'package:sendyshop/provider/product_provider.dart';
import 'package:sendyshop/provider/profile_provider.dart';
import 'package:sendyshop/provider/search_provider.dart';
import 'package:sendyshop/provider/seller_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Core
  sl.registerLazySingleton(() => NetworkInfo(sl()));
  sl.registerLazySingleton(() => DioClient());

  // Repository
  sl.registerLazySingleton(() => AuthRepo(sharedPreferences: sl()));
  sl.registerLazySingleton(() => BrandRepo());
  sl.registerLazySingleton(() => CartRepo(sharedPreferences: sl()));
  sl.registerLazySingleton(() => CategoryRepo());
  sl.registerLazySingleton(() => LocationRepo(sharedPreferences: sl()));
  sl.registerLazySingleton(() => OnboardingRepo());
  sl.registerLazySingleton(() => OrderRepo());
  sl.registerLazySingleton(() => ProductDetailsRepo());
  sl.registerLazySingleton(() => ProductRepo());
  sl.registerLazySingleton(() => ProfileRepo(sharedPreferences: sl()));
  sl.registerLazySingleton(() => SearchRepo(sharedPreferences: sl()));
  sl.registerLazySingleton(() => SellerRepo());
  sl.registerLazySingleton(() => ThemeRepo(sharedPreferences: sl()));

  // Provider
  sl.registerFactory(
      () => AuthProvider(sharedPreferences: sl(), authRepo: sl()));
  sl.registerFactory(() => BrandProvider(brandRepo: sl()));
  sl.registerFactory(() => CartProvider(cartRepo: sl()));
  sl.registerFactory(() => CategoryProvider(categoryRepo: sl()));
  sl.registerFactory(
      () => LocationProvider(sharedPreferences: sl(), locationRepo: sl()));
  sl.registerFactory(() => OnboardingProvider(onboardingRepo: sl()));
  sl.registerFactory(() => OrderProvider(orderRepo: sl()));
  sl.registerFactory(() => ProductDetailsProvider(productDetailsRepo: sl()));
  sl.registerFactory(() => ProductProvider(productRepo: sl()));
  sl.registerFactory(() => ProfileProvider(profileRepo: sl()));
  sl.registerFactory(() => SearchProvider(searchRepo: sl()));
  sl.registerFactory(() => SellerProvider(sellerRepo: sl()));
  sl.registerFactory(() => ThemeProvider(themeRepo: sl()));

  // External
  final sharedPreferences = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPreferences);
  sl.registerLazySingleton(() => Dio());
  sl.registerLazySingleton(() => LoggingInterceptor());
  sl.registerLazySingleton(() => DataConnectionChecker());
}
