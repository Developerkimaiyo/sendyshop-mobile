import 'package:flutter/material.dart';
import 'package:sendyshop/views/screens/auth/auth_screen.dart';
import 'package:sendyshop/views/screens/auth/forget_password_screen.dart';
import 'package:sendyshop/views/screens/main/bottom_bar.dart';
import 'package:sendyshop/util/app_route.dart';
import 'package:sendyshop/views/screens/onboarding/sendy_onboarding_screen.dart';

class RouteGenerator {
  static Route<dynamic> generatedRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case AppRoutes.ONBOARDING_SCREEN:
        return MaterialPageRoute(
            builder: (context) => SendyShopOnBoardingScreen());
      case AppRoutes.MAIN_SCREEN:
        return MaterialPageRoute(builder: (context) => BottomBarScreen());
      case AppRoutes.AUTH_SCREEN:
        return MaterialPageRoute(builder: (context) => AuthScreen());
      case AppRoutes.FORGET_PASSWORD_SCREEN:
        return MaterialPageRoute(builder: (context) => ForgetPasswordScreen());
      default:
        return MaterialPageRoute(
            builder: (context) => Scaffold(
                  body: SafeArea(
                    child: Center(
                      child: Text("error"),
                    ),
                  ),
                ));
    }
  }
}
