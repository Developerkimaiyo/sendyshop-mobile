class Images {
  static const String placeholder_image = 'assets/images/placeholder3.png';

  static const String user_image = 'assets/images/user.jpg';
  static const String cart_image = 'assets/images/cart.png';
  static const String toolbar_background = 'assets/images/tool_bar.jpg';
  static const String like = 'assets/images/like.png';
  static const String unlike = 'assets/images/unlike.png';
  static const String login = 'assets/images/user_login.png';
  static const String no_internet = 'assets/images/opps_internet.png';
  static const String no_data = 'assets/images/shopping_cart.png';
  static const String home_image = 'assets/images/home.png';
  static const String bag = 'assets/images/bag.png';
  static const String more_image = 'assets/images/more.png';
  static const String EDIT_TWO = 'assets/images/pencil.png';
  static const String background = 'assets/images/background.jpg';

  static const String user =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623530573/products/profile_epuapr.png';
  static const String category_one =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409817/sendyshop/makeup_ydgp5o.png';
  static const String category_two =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409817/sendyshop/workspace_kz5563.png';
  static const String category_three =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409817/sendyshop/mobile_xukepp.png';
  static const String category_four =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409818/sendyshop/comp_ofqish.png';
  static const String category_five =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409817/sendyshop/electronics_d2ke4j.png';
  static const String category_six =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409818/sendyshop/clothes_un8tvv.png';
  static const String category_seven =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409817/sendyshop/toys_kdgynu.png';
  static const String category_eight =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623409817/sendyshop/grocery_vnmfez.png';

  static const String beauty =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459703/products/lipstic_dx8cwd.png';
  static const String office =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459704/products/chair-33576_1280_gzyup6.png';
  static const String phone =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459701/products/mobile-phone-2198770_1280_tifezv.png';
  static const String computer =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459844/products/apple-606761_1920_h4vrfz.jpg';
  static const String electronic =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459707/products/refrigerator-2420419_1920_lbws3e.png';
  static const String fashion =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459703/products/neckties-210347_1920_ngnp8s.jpg';
  static const String babyProduts =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459702/products/toy_kat1eg.jpg';
  static const String groceries =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623459702/products/vegetables-1238252_1920_yzzvwr.jpg';

  static const String person =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623530794/products/profile-user_y9qdyh.png';

  static const String person2 =
      'https://res.cloudinary.com/dwlhubxxu/image/upload/v1623530794/products/profile-user_y9qdyh.png';

  //carousel
  static const String banner1 = 'assets/images/banner/banner1.jpg';
  static const String banner2 = 'assets/images/banner/banner2.png';
  static const String banner3 = 'assets/images/banner/banner5.jpg';
  static const String banner4 = 'assets/images/banner/banner4.png';

//brands
  static const String brand =
      'https://firebasestorage.googleapis.com/v0/b/am-uikit.appspot.com/o/Demo%20Image%2FsixValley%2Fbrand.jpg?alt=media&token=43155d42-c5aa-4f28-95ba-683c2dd09250';
  static const String brand_one =
      'https://firebasestorage.googleapis.com/v0/b/am-uikit.appspot.com/o/Demo%20Image%2FsixValley%2Fbrand1.png?alt=media&token=459470ad-0cab-4a81-b9a5-4ddbff624fcc';
  static const String brand_two =
      'https://firebasestorage.googleapis.com/v0/b/am-uikit.appspot.com/o/Demo%20Image%2FsixValley%2Fbrand2.png?alt=media&token=29799b9f-5204-4284-a953-45aefb8a2513';
  static const String brand_three =
      'https://firebasestorage.googleapis.com/v0/b/am-uikit.appspot.com/o/Demo%20Image%2FsixValley%2Fbrand3.png?alt=media&token=07973b64-16d1-48ab-ba94-be1ddb7cda80';
  static const String brand_four =
      'https://firebasestorage.googleapis.com/v0/b/am-uikit.appspot.com/o/Demo%20Image%2FsixValley%2Fbrand4.png?alt=media&token=b7873e5b-4b26-4bd9-853b-24fd52d23b84';

  static const String paypal = 'assets/images/paypal.png';
  static const String mastercard = 'assets/images/mastercard.png';
  static const String visa = 'assets/images/about_us.png';
  static const String mpesa = 'assets/images/mpesa.png';
  static const String binoculars = 'assets/images/binoculars.png';
  static const String clock = 'assets/images/clock.png';
  static const String marker = 'assets/images/marker.png';
  static const String filter_image = 'assets/images/filter.png';
  static const String profile = 'assets/images/profile.png';
  static const String log_out = 'assets/images/log_out.png';

  static const String logo = 'assets/images/sendyshop2.png';

  static const String onBoard_one = 'assets/images/onboarding_one.png';
  static const String onBoard_two = 'assets/images/splash_image.png';
  static const String onBoard_three = 'assets/images/onboarding_two.png';
}
