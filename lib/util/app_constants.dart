class AppConstants {
  static const String BASE_URL = 'http://1f8669e76d74.ngrok.io';

  static const String THEME = 'theme';
  static const String CURRENCY = 'Ksh.';
  static const String EXCHANGERATE = '1.2';
  static const String CART_LIST = 'cart_list';

  // sharePreference
  static const String TOKEN = 'token';
  static const String USER_EMAIL = 'user_email';
  static const String USER_PASSWORD = 'user_password';
  static const String HOME_ADDRESS = 'home_address';
  static const String SEARCH_ADDRESS = 'search_address';
  static const String OFFICE_ADDRESS = 'office_address';
  static const String CONFIG = 'config';
  static const String COUNTRY_CODE = 'country_code';
  static const String LANGUAGE_CODE = 'language_code';
  static const String USER_ADDRESS = 'user_address';
  static const String USER_NUMBER = 'user_number';
  static const String FAVOURITE_LIST = 'favourite_list';
  static const String ALL_FAVOURITE_LIST = 'all_favourite_list';

  // for onboarding screen
  static const String online_shopping = 'Shop With Us';
  static const String shopping = 'Shopping Now';
  static const String order = 'Maker Your Order';
  static const String address = 'Ship at Your Home';
  static const String detail1 =
      'The products you order will be delivered to your address';
  static const String detail2 =
      'Start shopping anywhere, anytime and anything \nthat you want.';
  static const String detail3 =
      'We provide a range of quality products to make a choice from';
}
