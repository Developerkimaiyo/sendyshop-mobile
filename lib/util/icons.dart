import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';

class MyIcons {
  static IconData rss = Feather.rss;
  static IconData search = Feather.search;
  static IconData user = Feather.user;
  static IconData cart = MaterialCommunityIcons.cart;

  static IconData edit = MaterialCommunityIcons.table_edit;

  static IconData trash = Feather.trash;
  static IconData home = Icons.home;

  static IconData order = Icons.shopping_bag;
  static IconData favorite = Icons.favorite;
  static IconData account = MyIcons.user;

  static IconData wishlist = Ionicons.ios_heart_empty;
  static IconData upload = Feather.upload;
}
