class AppRoutes {
  static const String ONBOARDING_SCREEN = 'onboarding';
  static const String MAIN_SCREEN = 'main';
  static const String AUTH_SCREEN = 'authentication';
  static const String FORGET_PASSWORD_SCREEN = 'forgetpassword';
}
