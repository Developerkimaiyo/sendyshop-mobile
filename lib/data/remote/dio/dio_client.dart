import 'dart:io';

import 'package:dio/dio.dart';
import 'package:sendyshop/data/remote/dio/logging_interceptor.dart';
import 'package:sendyshop/helper/shareprefs.dart';
import 'package:sendyshop/util/app_constants.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:cookie_jar/cookie_jar.dart';

class DioClient {
  String baseUrl;
  LoggingInterceptor loggingInterceptor = LoggingInterceptor();

  CookieJar cookieJar;
  Dio dio;
  String token;

  DioClient() {
    token = SharedPrefs.sharedPreferences.getString(AppConstants.TOKEN);
    cookieJar = new CookieJar();
    dio = Dio();
    dio
      ..options.baseUrl = AppConstants.BASE_URL
      ..options.connectTimeout = 30000
      ..options.receiveTimeout = 30000
      ..httpClientAdapter
      ..options.headers = {
        HttpHeaders.userAgentHeader: "dio",
        "Connection": "keep-alive",
      };
    dio.interceptors.add(InterceptorsWrapper(onRequest: (options, handler) {
      options.headers.addAll({'Authorization': 'Bearer $token'});
      return handler.next(options);
    }));

    dio.interceptors.add(loggingInterceptor);
    dio.interceptors.add(CookieManager(cookieJar));
  }

  Future<Response> get(
    String uri, {
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var response = await dio.get(
        uri,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status <= 503;
          },
        ),
        cancelToken: cancelToken,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on SocketException catch (e) {
      throw SocketException(e.toString());
    } on FormatException catch (_) {
      throw FormatException("Unable to process the data");
    } catch (e) {
      throw e;
    }
  }

  Future<Response> post(
    String uri, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var response = await dio.post(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status <= 503;
          },
        ),
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on FormatException catch (_) {
      throw FormatException("Unable to process the data");
    } catch (e) {
      throw e;
    }
  }

  Future<Response> put(
    String uri, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
    ProgressCallback onSendProgress,
    ProgressCallback onReceiveProgress,
  }) async {
    try {
      var response = await dio.put(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status <= 503;
          },
        ),
        cancelToken: cancelToken,
        onSendProgress: onSendProgress,
        onReceiveProgress: onReceiveProgress,
      );
      return response;
    } on FormatException catch (_) {
      throw FormatException("Unable to process the data");
    } catch (e) {
      throw e;
    }
  }

  Future<Response> delete(
    String uri, {
    data,
    Map<String, dynamic> queryParameters,
    Options options,
    CancelToken cancelToken,
  }) async {
    try {
      var response = await dio.delete(
        uri,
        data: data,
        queryParameters: queryParameters,
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status <= 503;
          },
        ),
        cancelToken: cancelToken,
      );
      return response;
    } on FormatException catch (_) {
      throw FormatException("Unable to process the data");
    } catch (e) {
      throw e;
    }
  }
}
