class Product {
  int id;
  int userId;
  int categoryId;
  int brandId;
  String name;
  String thumbnail;
  String unitPrice;
  String purchasePrice;
  String tax;
  String taxType;
  String discount;
  String discountType;
  String currentStock;
  String details;
  String createdAt;
  String updatedAt;
  String status;

  Product({
    this.id,
    this.userId,
    this.name,
    this.categoryId,
    this.brandId,
    this.thumbnail,
    this.unitPrice,
    this.purchasePrice,
    this.tax,
    this.taxType,
    this.discount,
    this.discountType,
    this.currentStock,
    this.details,
    this.createdAt,
    this.updatedAt,
    this.status,
  });

  Product.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    categoryId = json['category_id'];
    brandId = json['brand_id'];
    thumbnail = json['thumbnail'];
    unitPrice = json['unit_price'];
    purchasePrice = json['purchase_price'];
    tax = json['tax'];
    taxType = json['tax_type'];
    discount = json['discount'];
    discountType = json['discount_type'];
    currentStock = json['current_stock'];
    details = json['details'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['name'] = this.name;
    data['brand_id'] = this.brandId;
    data['category_id'] = this.categoryId;
    data['thumbnail'] = this.thumbnail;
    data['unit_price'] = this.unitPrice;
    data['purchase_price'] = this.purchasePrice;
    data['tax'] = this.tax;
    data['tax_type'] = this.taxType;
    data['discount'] = this.discount;
    data['discount_type'] = this.discountType;
    data['current_stock'] = this.currentStock;
    data['details'] = this.details;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['status'] = this.status;
    return data;
  }
}
