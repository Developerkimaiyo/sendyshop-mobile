class Category {
  int id;
  String name;
  String icon;
  String createdAt;
  String updatedAt;
  List<SubCategory> subCategories;

  Category(
      {this.id,
      this.name,
      this.icon,
      this.createdAt,
      this.updatedAt,
      this.subCategories});

  Category.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['childes'] != null) {
      subCategories = [];
      json['childes'].forEach((v) {
        subCategories.add(new SubCategory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['icon'] = this.icon;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.subCategories != null) {
      data['childes'] = this.subCategories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SubCategory {
  int id;
  String name;
  String icon;
  String createdAt;
  String updatedAt;

  List<SubSubCategory> subSubCategories;

  SubCategory(
      {this.id,
      this.name,
      this.icon,
      this.createdAt,
      this.updatedAt,
      this.subSubCategories});

  SubCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['childes'] != null) {
      subSubCategories = [];
      json['childes'].forEach((v) {
        subSubCategories.add(new SubSubCategory.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['icon'] = this.icon;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    if (this.subSubCategories != null) {
      data['childes'] = this.subSubCategories.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SubSubCategory {
  int id;
  String name;
  String icon;
  String createdAt;
  String updatedAt;

  SubSubCategory({
    this.id,
    this.name,
    this.icon,
    this.createdAt,
    this.updatedAt,
  });

  SubSubCategory.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['icon'] = this.icon;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
