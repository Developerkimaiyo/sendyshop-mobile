class ShippingModel {
  int _id;
  double _shippingCost;
  String _createdAt;
  String _updatedAt;

  ShippingModel(
      {int id, double shippingCost, String createdAt, String updatedAt}) {
    this._id = id;
    this._shippingCost = shippingCost;
    this._createdAt = createdAt;
    this._updatedAt = updatedAt;
  }

  int get id => _id;
  double get shippingCost => _shippingCost;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  ShippingModel.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _shippingCost = json['shippingCost'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['shippingCost'] = this._shippingCost;
    data['created_at'] = this._createdAt;
    data['updated_at'] = this._updatedAt;
    return data;
  }
}
