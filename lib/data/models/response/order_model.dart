class OrderModel {
  int id;
  int userId;
  String orderAmount;
  String paymentStatus;
  String orderStatus;
  String totalTaxAmount;
  String deliveryAddressId;
  String createdAt;
  String updatedAt;
  List<OrderItem> orderItem;

  OrderModel({
    this.id,
    this.userId,
    this.orderAmount,
    this.paymentStatus,
    this.orderStatus,
    this.totalTaxAmount,
    this.deliveryAddressId,
    this.createdAt,
    this.updatedAt,
    this.orderItem,
  });

  OrderModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    orderAmount = json['order_amount'];
    paymentStatus = json['payment_status'];
    orderStatus = json['order_status'];
    totalTaxAmount = json['total_tax_amount'];
    deliveryAddressId = json['delivery_address_id'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    if (json['order_items'] != null) {
      orderItem = [];
      json['order_items'].forEach((v) {
        orderItem.add(new OrderItem.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['order_amount'] = this.orderAmount;
    data['payment_status'] = this.paymentStatus;
    data['order_status'] = this.orderStatus;
    data['total_tax_amount'] = this.totalTaxAmount;
    data['delivery_address_id'] = this.deliveryAddressId;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;

    if (this.orderItem != null) {
      data['order_items'] = this.orderItem.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class OrderItem {
  int id;
  String productId;
  String orderId;
  String price;
  String productName;
  String image;
  String discount;
  String quantity;
  String taxAmount;
  String createdAt;
  String updatedAt;

  OrderItem({
    this.id,
    this.productId,
    this.orderId,
    this.price,
    this.productName,
    this.image,
    this.discount,
    this.quantity,
    this.taxAmount,
    this.createdAt,
    this.updatedAt,
  });

  OrderItem.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    productId = json['product_id'];
    orderId = json['order_id'];
    price = json['price'];
    productName = json['product_name'];
    image = json['image'];
    discount = json['discount'];
    quantity = json['quantity'];
    taxAmount = json['tax_amount'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['product_id'] = this.productId;
    data['order_id'] = this.orderId;
    data['price'] = this.price;
    data['product_name'] = this.productName;
    data['image'] = this.image;
    data['discount'] = this.discount;
    data['quantity'] = this.quantity;
    data['tax_amount'] = this.taxAmount;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}
