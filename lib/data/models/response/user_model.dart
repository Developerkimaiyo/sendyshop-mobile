class UserModel {
  String fName;
  String lName;
  String email;
  String password;
  String phone;
  String image;
  String createdAt;
  String updatedAt;

  UserModel(
      {this.fName,
      this.lName,
      this.email,
      this.password,
      this.phone,
      this.image,
      this.createdAt,
      this.updatedAt});

  UserModel.fromJson(Map<String, dynamic> json) {
    fName = json['f_name'];
    lName = json['l_name'];
    email = json['email'];
    password = json['password'];
    phone = json['phone'];
    image = json['image'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['f_name'] = this.fName;
    data['l_name'] = this.lName;
    data['phone'] = this.phone;
    data['password'] = this.password;
    data['email'] = this.email;
    data['image'] = image;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
