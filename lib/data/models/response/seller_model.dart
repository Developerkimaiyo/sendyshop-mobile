class SellerModel {
  int id;
  String fName;
  String lName;
  String email;
  String phone;
  String image;
  String shop;

  SellerModel({
    this.id,
    this.fName,
    this.lName,
    this.email,
    this.phone,
    this.image,
    this.shop,
  });

  SellerModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fName = json['f_name'];
    lName = json['l_name'];
    email = json['email'];
    phone = json['phone'];
    image = json['image'];
    shop = json['shop'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['f_name'] = this.fName;
    data['l_name'] = this.lName;
    data['phone'] = this.phone;
    data['image'] = this.image;
    data['email'] = this.email;
    data['shop'] = this.shop;

    return data;
  }
}
