class OrderPlaceModel {
  List<Cart> _cart;
  String _paymentMethod;
  double _orderAmount;
  int _deliveryAddressId;

  OrderPlaceModel({
    List<Cart> cart,
    String paymentMethod,
    double orderAmount,
    int deliveryAddressId,
  }) {
    this._deliveryAddressId = deliveryAddressId;
    this._cart = cart;
    this._paymentMethod = paymentMethod;
    this._orderAmount = orderAmount;
  }

  int get deliveryAddressId => deliveryAddressId;
  List<Cart> get cart => _cart;
  String get paymentMethod => _paymentMethod;
  double get orderAmount => _orderAmount;
  OrderPlaceModel.fromJson(Map<String, dynamic> json) {
    if (json['cart'] != null) {
      _cart = [];
      json['cart'].forEach((v) {
        _cart.add(new Cart.fromJson(v));
      });
    }
    _paymentMethod = json['payment_method'];
    _orderAmount = json['order_amount'];
    _deliveryAddressId = json['delivery_address_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._cart != null) {
      data['cart'] = this._cart.map((v) => v.toJson()).toList();
    }
    data['payment_method'] = this._paymentMethod;
    data['order_amount'] = this._orderAmount;
    data['delivery_address_id'] = this._deliveryAddressId;
    return data;
  }
}

class Cart {
  String _id;
  int _tax;
  int _quantity;
  int _price;
  int _discount;
  String _discountType;
  int _shippingCost;

  Cart(String id, int tax, int quantity, int price, int discount,
      String discountType, int shippingCost) {
    this._id = id;
    this._tax = tax;
    this._quantity = quantity;
    this._price = price;
    this._discount = discount;
    this._discountType = discountType;
    this._shippingCost = shippingCost;
  }

  String get id => _id;
  int get tax => _tax;
  int get quantity => _quantity;
  int get price => _price;
  int get discount => _discount;
  String get discountType => _discountType;
  int get shippingCost => _shippingCost;

  Cart.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _tax = json['tax'];
    _quantity = json['quantity'];
    _price = json['price'];
    _discount = json['discount'];
    _discountType = json['discount_type'];
    _shippingCost = json['shipping_cost'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['tax'] = this._tax;
    data['quantity'] = this._quantity;
    data['price'] = this._price;
    data['discount'] = this._discount;
    data['discount_type'] = this._discountType;
    data['shipping_cost'] = this._shippingCost;
    return data;
  }
}
