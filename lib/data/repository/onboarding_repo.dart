import 'package:sendyshop/data/models/response/onboarding_model.dart';
import 'package:sendyshop/util/app_constants.dart';
import 'package:sendyshop/util/images.dart';

class OnboardingRepo {
  List<OnBoardingModel> getOnboardingList = [
    OnBoardingModel(
      imageUrl: Images.onBoard_one,
      title: AppConstants.online_shopping,
      subTitle: AppConstants.detail2,
    ),
    OnBoardingModel(
      imageUrl: Images.onBoard_two,
      title: AppConstants.order,
      subTitle: AppConstants.detail3,
    ),
    OnBoardingModel(
      imageUrl: Images.onBoard_three,
      title: AppConstants.address,
      subTitle: AppConstants.detail1,
    ),
  ];
}
