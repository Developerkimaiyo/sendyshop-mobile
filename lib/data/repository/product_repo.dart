import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/exception/api_error_handler.dart';

class ProductRepo {
  DioClient _client = DioClient();
  Future<ApiResponse> getLatestProductList() async {
    try {
      final response = await _client.get('/api/latestproducts');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getSellerProductList(String id) async {
    try {
      final response = await _client.get('/api/sellerproducts?userId=${id}');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getCategoryProductList(String id) async {
    try {
      final response =
          await _client.get('/api/categoryProducts?categoryId=${id}');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getBrandProductList(String id) async {
    try {
      final response = await _client.get('/api/brandProducts?brandId=${id}');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  List<Product> geProductList() {
    List<Product> productList = [];
    return productList;
  }

  Future<ApiResponse> getRelatedProductList(String id) async {
    try {
      final response =
          await _client.get("/api/relatedproducts?categoryId=${id}");
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
