import 'package:dio/dio.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/models/response/order_details.dart';
import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/data/models/response/shipping_model.dart';
import 'package:sendyshop/data/models/response/order_model.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/exception/api_error_handler.dart';

class OrderRepo {
  DioClient _client = DioClient();
  Future<ApiResponse> getProcessingOrder() async {
    try {
      final response =
          await _client.get('/api/processing?processing=processing');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getTransitOrder() async {
    try {
      final response = await _client.get('/api/transit?transit=transit');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getDeliveredOrder() async {
    try {
      final response = await _client.get('/api/delivered?delivered=delivered');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getCancelOrder() async {
    try {
      final response = await _client.get('/api/cancel?cancel=cancel');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getOrder() async {
    try {
      final response = await _client.get('/api/order');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> updateStatus(data, id) async {
    try {
      final response = await _client.put('/api/status/${id}',
          options: Options(contentType: 'multipart/form-data'), data: data);

      return ApiResponse.withSuccess(response);
    } catch (error) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(error));
    }
  }

  Future<ApiResponse> deleteOrder(id) async {
    try {
      final response = await _client.delete("/api/order/${id}");
      print(response.data);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getOrderDetails(String orderID) async {
    try {
      List<OrderDetailsModel> _orderDetailsList = [
        OrderDetailsModel(
            id: 1,
            orderId: '100001',
            productId: '1',
            price: 100,
            sellerId: '2',
            productDetails: Product(),
            qty: 2,
            discount: 10,
            tax: 20,
            createdAt: '2021-01-02T06:11:23.000000Z'),
      ];
      final response = Response(
          requestOptions: RequestOptions(path: ''),
          data: _orderDetailsList,
          statusCode: 200);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> trackOrder(String orderID) async {
    try {
      OrderModel _orderModel = OrderModel(
          id: 1,
          userId: 1,
          deliveryAddressId: '1',
          orderStatus: 'pending',
          updatedAt: '2021-01-02T06:11:23.000000Z');
      final response = Response(
          requestOptions: RequestOptions(path: ''),
          data: _orderModel,
          statusCode: 200);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  ShippingModel getShipingCost() {
    ShippingModel shipingCost = ShippingModel(id: 1, shippingCost: 250.00);
    return shipingCost;
  }
}
