import 'package:dio/dio.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/exception/api_error_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileRepo {
  final SharedPreferences sharedPreferences;
  ProfileRepo({this.sharedPreferences});
  DioClient _client = DioClient();

  Future<ApiResponse> getUserInfo() async {
    try {
      final response = await _client.get('/api/user',
          options: Options(contentType: 'multipart/form-data'));
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> upDateUser(data) async {
    try {
      final response = await _client.put('/api/update',
          options: Options(contentType: 'multipart/form-data'), data: data);

      return ApiResponse.withSuccess(response);
    } catch (error) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(error));
    }
  }
}
