import 'package:sendyshop/util/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ThemeRepo {
  final SharedPreferences sharedPreferences;
  ThemeRepo({this.sharedPreferences});
  setDarkTheme(bool value) async {
    sharedPreferences.setBool(AppConstants.THEME, value);
  }

  Future<bool> getTheme() async {
    return sharedPreferences.getBool(
          AppConstants.THEME,
        ) ??
        false;
  }
}
