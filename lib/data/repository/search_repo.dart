import 'package:sendyshop/data/models/response/product_model.dart';
import 'package:sendyshop/util/app_constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SearchRepo {
  final SharedPreferences sharedPreferences;
  SearchRepo({this.sharedPreferences});

  List<Product> getSearchProductList() {
    List<Product> productList = [];
    return productList;
  }

  // for save home address
  Future<void> saveSearchAddress(String searchAddress) async {
    try {
      List<String> searchKeywordList =
          sharedPreferences.getStringList(AppConstants.SEARCH_ADDRESS);
      if (!searchKeywordList.contains(searchAddress)) {
        searchKeywordList.add(searchAddress);
      }
      await sharedPreferences.setStringList(
          AppConstants.SEARCH_ADDRESS, searchKeywordList);
    } catch (e) {
      throw e;
    }
  }

  List<String> getSearchAddress() {
    return sharedPreferences.getStringList(AppConstants.SEARCH_ADDRESS) ?? [];
  }

  Future<bool> clearSearchAddress() async {
    return sharedPreferences.setStringList(AppConstants.SEARCH_ADDRESS, []);
  }
}
