import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/exception/api_error_handler.dart';

class BrandRepo {
  DioClient _client = DioClient();
  Future<ApiResponse> getBrandList() async {
    try {
      final response = await _client.get('/api/brand');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
