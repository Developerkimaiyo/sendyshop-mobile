import 'package:dio/dio.dart';
import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/exception/api_error_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocationRepo {
  final SharedPreferences sharedPreferences;
  LocationRepo({this.sharedPreferences});
  DioClient _client = DioClient();

  Future<ApiResponse> getAllAddress() async {
    try {
      final response = await _client.get('/api/address');
      print(response.data);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> getAddress(id) async {
    try {
      final response = await _client.get("/api/address/${id}");
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> deleteAddress(id) async {
    try {
      final response = await _client.delete("/api/address/${id}");
      print(response.data);
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }

  Future<ApiResponse> upDataAddress(data, id) async {
    try {
      final response = await _client.put("/api/address/${id}",
          options: Options(contentType: 'multipart/form-data'), data: data);

      return ApiResponse.withSuccess(response);
    } catch (error) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(error));
    }
  }

  Future<ApiResponse> createAddress(data) async {
    try {
      final response = await _client.post('/api/address', data: data);

      return ApiResponse.withSuccess(response);
    } catch (error) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(error));
    }
  }
}
