import 'package:sendyshop/data/models/response/base/api_response.dart';
import 'package:sendyshop/data/remote/dio/dio_client.dart';
import 'package:sendyshop/data/remote/exception/api_error_handler.dart';

class CategoryRepo {
  DioClient _client = DioClient();
  Future<ApiResponse> getCategoryList() async {
    try {
      final response = await _client.get('/api/category');
      return ApiResponse.withSuccess(response);
    } catch (e) {
      return ApiResponse.withError(ApiErrorHandler.getMessage(e));
    }
  }
}
