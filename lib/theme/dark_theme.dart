import 'package:flutter/material.dart';

ThemeData DarkData = ThemeData(
  fontFamily: 'Rubik',
  primaryColor: Color(0xFFD85F15),
  brightness: Brightness.dark,
  scaffoldBackgroundColor: Color(0xFF2C2C2C),
  accentColor: Color(0xFF252525),
  hintColor: Color(0xFFE7F6F8),
  focusColor: Color(0xFFADC4C8),
);
