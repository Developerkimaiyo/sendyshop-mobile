import 'package:flutter/material.dart';

ThemeData LightData = ThemeData(
  fontFamily: 'Rubik',
  primaryColor: Color(0xFFD85F15),
  brightness: Brightness.light,
  accentColor: Colors.white,
  focusColor: Color(0xFFADC4C8),
  hintColor: Color(0xFF52575C),
);
