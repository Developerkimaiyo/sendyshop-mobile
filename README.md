# Sendy Shop



| Image 1    | Image 2     | Image 3     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134055/appflutter/Screenshot_20210701-121512_dhq7b5.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134055/appflutter/Screenshot_20210701-124216_vbkoou.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134055/appflutter/Screenshot_20210701-124223_t97xn7.png" width="250"> |

| Image 4    | Image 5     | Image 6     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134055/appflutter/Screenshot_20210701-124226_lc7xit.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134054/appflutter/Screenshot_20210701-124234_bdag4s.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134054/appflutter/Screenshot_20210701-124243_edwqnn.png" width="250"> |

| Image 7    | Image 8     | Image 9     |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134054/appflutter/Screenshot_20210701-125534_ihcwar.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134054/appflutter/Screenshot_20210701-125537_ddlaus.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134053/appflutter/Screenshot_20210701-125549_ihqley.png" width="250"> |

| Image 10    | Image 11     | Image 12  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134054/appflutter/Screenshot_20210701-125626_fiwp00.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134053/appflutter/Screenshot_20210701-125559_ilqkvs.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134053/appflutter/Screenshot_20210701-125614_r5wfxw.png" width="250"> |

| Image 13    | Image 14     | Image 15  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134052/appflutter/Screenshot_20210701-125645_tetz7v.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134052/appflutter/Screenshot_20210701-125659_l2nmcd.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134052/appflutter/Screenshot_20210701-125707_evtulv.png" width="250"> |
| Image 16    | Image 17     | Image 18  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134052/appflutter/Screenshot_20210701-125730_lbcfl1.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134052/appflutter/Screenshot_20210701-125740_x49rgd.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134052/appflutter/Screenshot_20210701-125754_ahzunt.png" width="250"> |
| Image 19    | Image 20     | Image 22  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134051/appflutter/Screenshot_20210701-125810_jxyoay.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134050/appflutter/Screenshot_20210701-125830_fak4yh.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134051/appflutter/Screenshot_20210701-125843_hedpjt.png" width="250"> |
| Image 23    | Image 24     | Image 25  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134051/appflutter/Screenshot_20210701-125904_pl3dcp.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134051/appflutter/Screenshot_20210701-125909_kqjmvp.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134051/appflutter/Screenshot_20210701-125925_xslcqo.png" width="250"> |
| Image 26    | Image 27     | Image 28  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134049/appflutter/Screenshot_20210701-125940_drkqpd.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134050/appflutter/Screenshot_20210701-130002_q3f2jq.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134049/appflutter/Screenshot_20210701-130145_pimumk.png" width="250"> |
| Image 29    | Image 30     | Image 31  |
|------------|-------------|-------------|
| <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134049/appflutter/Screenshot_20210701-130200_ax6hzl.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134049/appflutter/Screenshot_20210701-130109_g4j6ja.png" width="250"> | <img src="https://res.cloudinary.com/dwlhubxxu/image/upload/v1625134049/appflutter/Screenshot_20210701-125945_xl8af3.png" width="250"> |




### Support

Reach out to me at one of the following places!

- Twitter at <a href="http://twitter.com/maxxmalakwen" target="_blank">`@maxxmalakwen`</a>

Let me know if you have any questions. Email me At maxwell@sendyit.com or developerkimaiyo@gmail.com



---

### License

[![License](http://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**
- Copyright 2021 © <a href="https://github.com/Developer-Kimaiyo" target="_blank">Maxwell Kimaiyo</a>.