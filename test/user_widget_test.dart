import 'package:flutter_test/flutter_test.dart';
import 'package:list_tile_switch/list_tile_switch.dart';
import 'package:sendyshop/views/screens/account/account_screen.dart';

void main() {
  testWidgets('user widget test', (WidgetTester tester) async {
    await tester.pumpWidget(AccountScreen(
      onTap: null,
    ));
    var switchButton = find.byType(ListTileSwitch);
    expect(switchButton, findsOneWidget);
    await tester.tap(switchButton, warnIfMissed: true);
    await tester.pump();
    print('tapped');
  });
}
