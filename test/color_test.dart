import 'dart:ui';

import 'package:flutter_test/flutter_test.dart';
import 'package:sendyshop/helper/ui.dart';

void main() {
  test('should hex color to color object', () {
    String initial = "#ea7125";
    Color expected = Ui.colorFromHex(initial);

    expect(expected, Color(0xffea7125));
  });
}
